require.config({
    paths: {
        domReady: 'node_modules/dom-ready/domReady',
        angular: 'node_modules/angular/angular',
        angularRoute: 'node_modules/angular-route/angular-route.min',
        angularSanitize: 'node_modules/angular-sanitize/angular-sanitize.min',
        jQuery: 'node_modules/jquery/dist/jquery.min',
        bootstrap: 'node_modules/bootstrap/dist/js/bootstrap.min',
        angularBootstrap: 'node_modules/angular-ui-bootstrap/ui-bootstrap-tpls.min',
        //angularAnimate: 'Libraries/angular-animate',
        angularFileUpload: 'node_modules/angular-file-upload/dist/angular-file-upload.min',
        moment: 'node_modules/moment/min/moment.min',
        jQueryUi: 'node_modules/jquery-ui/jquery-ui',
        fullPage: 'node_modules/fullpage.js/jquery.fullPage'
    },
    shim: {
        jQuery : {
            exports: 'jQuery'
        },
        jQueryUi: {
            deps: ['jQuery']
        },
        fullPage: {
            deps: ['jQuery']
        },
        angular : {
            exports: 'angular'
        },
        angularRoute: {
            deps: ['angular']
        },
        angularSanitize: {
            deps: ['angular']
        },
        bootstrap: {
            deps: ['jQuery']
        },
        angularBootstrap: {
            deps: ['bootstrap','angular']
        },
        //angularAnimate: {
        //    deps: ['angular']
        //},
        angularFileUpload: {
            deps: ['angular', 'bootstrap']
        },
        moment: {
            exports: 'moment'
        }
    },
    deps: ['bootstrapApp', 'node_modules/less/dist/less.min']
});