/**
 * Created by Guille on 25/09/2014.
 */
define([
    'require',
    'angular',
    './app',
    'modules/model/model',
    'modules/api/api',
    'modules/views/views',
    'modules/suit/suit',
    './routes'
], function (require, angular) {
    'use strict';

    require(['domReady!'], function (document) {
        // start app
        angular.bootstrap(document, ['app' , 'model', 'api', 'views', 'suit']);
        // log
        console.log('App finished tdLoading');
    });
});