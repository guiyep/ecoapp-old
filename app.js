define([
    'angular',
    'angularRoute',
    'angularSanitize',
    'angularBootstrap',
    //'angularAnimate',
    'angularFileUpload',
    'jQueryUi',
    'modules/model/index',
    'modules/suit/index',
    'modules/api/index',
    'modules/views/index'
], function (angular) {
    'use strict';

    console.log('App Loaded');

    return angular.module('app', [
        'ngRoute',
        'ui.bootstrap',
        'ngSanitize',
        'angularFileUpload'
    ]);
});