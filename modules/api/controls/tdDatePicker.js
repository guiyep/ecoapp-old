/**
 * Created by guiyep on 15/09/15.
 */
define(['../api'], function (api) {
    'use strict';

    api.directive('tdDatePicker', ['tdEventRegisterService', 'tdParameterGetter', function (tdEventRegisterService, tdParameterGetter) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            require: 'ngModel',

            template: '<div class="col-md-6">' +
                            '<p class="input-group">' +
                                '<input type="text" ng-change="_changeDate()" ng-model="_date" class="td-date-picker-control form-control" datepicker-popup="dd-MMMM-yyyy" is-open="status.opened" datepicker-options="dateOptions" date-disabled="_disable"/>' +
                                '<span class="input-group-btn">' +
                                    '<button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar" ng-disabled="_disable"></i></button>' +
                                '</span>' +
                            '</p>' +
                    '</div>',

            controller: ['$scope', '$element', function ($scope, $element) {
                // deifne options
                $scope.dateOptions = {};
                // initialize status
                $scope.status = {
                    opened: false
                };
                // extend optiosn with the one defined in the controller if any
                angular.extend($scope.dateOptions, $scope.options);
                // get elements from attr and set options if any
                angular.extend($scope.dateOptions, tdParameterGetter.getParameters($element, {exclude: ['td-datepicker-popup', 'td-is-open', 'td-datepicker-options', 'td-date-disabled']}));
                // open function
                $scope.open = function () {
                    // set status as opened
                    $scope.status.opened = true;
                };

            }],

            compile: function (tElement, tAttrs) {
                // get element
                var input = $(tElement).find('.td-date-picker-control');
                // if we have set the optiosn
                if(tAttrs.tdDatepickerPopup)
                    // set ng model from ng model directive
                    input.attr('datepicker-popup', tAttrs.tdDatepickerPopup);
                // get custom options from attr and set to date input
                tdParameterGetter.setAttr(input, tdParameterGetter.getParameters(tElement), {exclude: ['td-datepicker-popup', 'td-is-open', 'td-datepicker-options', 'td-date-disabled']});
                // return link function
                return function (scope, element, attrs, ngModel) {
                    // define change functionality
                    scope._changeDate = function(){
                        // validate min date
                        if(attrs.tdMinDate && scope._date){
                            // min date
                            var min = scope.$eval(attrs.tdMinDate);
                            // validate min
                            if(min > scope._date)
                                // set model
                                return ngModel.$setViewValue(null, true);
                        }
                        // validate max date
                        if(attrs.tdMaxDate && scope._date){
                            // min date
                            var max = scope.$eval(attrs.tdMaxDate);
                            // validate min
                            if(max < scope._date)
                                // set model
                                return ngModel.$setViewValue(null, true);
                        }
                        // validate model is a date
                        if(angular.isDate(scope._date)){
                            // set model as touched
                            ngModel.$setTouched();
                            // set model
                            return ngModel.$setViewValue(angular.copy(scope._date), true);
                        }
                        // set model as null
                        return ngModel.$setViewValue(null, true);
                    };
                    // render function
                    ngModel.$render = function(){
                        // validate values is an object
                        if(angular.isDate(ngModel.$viewValue)) {
                            // set value
                            scope._date = ngModel.$viewValue;
                            // set model as touched
                            ngModel.$setTouched();
                        }
                    };
                    // initialize register service
                    var register = tdEventRegisterService(element);
                    // watch changes in the selection
                    register.listen(scope.$watch(
                        function () {
                            // watch on changes in the disabled state need to be evaluates each time
                            return element.attr('disabled');
                        }, function (value) {
                            // validate
                            scope._disable = (angular.isString(value) && (value === 'disabled'));
                        }
                    ));
                };
            }
        }
    }]);
});