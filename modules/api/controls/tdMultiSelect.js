/**
 * Created by guiyep on 31/08/15.
 */

define(['../api'], function (api) {
    'use strict';

    api.tdMultiSelect = {
        tdModelUpdated : 'td-multi-select-model-updated',
        tdAddItemCommand: 'td-multi-select-add-item',
        tdRemoveItemCommand: 'td-multi-select-remove-item',
        tdClearCommand: 'td-multi-select-clear'
    };

    api.directive('tdMultiSelect', ['tdEventRegisterService', '$log', '$timeout', '$q', function (tdEventRegisterService, $log, $timeout, $q) {
        return {
            // restrict to attribute or element or class name
            restrict: 'C',

            require: 'ngModel',

            template: '<div class="td-multi-select-input-selected" ng-repeat="item in _selectedItems">' +
                    '<label class="td-multi-select-input-label"></label>' +
                    '<div ng-click="removeItem(item, $index)" class="fa fa-minus-square fa-2x"></div>' +
                '</div>' +
                '<input class="form-control td-multi-select-input" ng-disabled="_disable" ng-model="_selectedItem" type="text">',

            controller: ['$scope', function($scope){
                // initialize selectedItems
                $scope._selectedItems = [];
            }],

            compile: function(tElement, tAttr){
                // validate we have all set
                if(tAttr.tdValueField && tAttr.tdDataField && tAttr.tdGetterFunction) {
                    // get input
                    var label = $(tElement).find('.td-multi-select-input-label');
                    // set label bind
                    label.attr('ng-bind', 'item.' + tAttr.tdValueField);
                    // get input
                    var input = $(tElement).find('.td-multi-select-input');
                    // set typeahead
                    input.attr('typeahead', 'obj as obj.' + tAttr.tdValueField + ' for obj in __search($viewValue)');
                }
                else{
                    $log.error('need attrs to be set')
                }
                // return link function
                return function(scope, element, attrs, ngModel){
                    // define commandas
                    var commands = {
                        tdAddItemCommand: scope.options && scope.options.tdAddItemCommand ? scope.options.tdAddItemCommand : (attrs.tdAddItemCommand ? attrs.tdAddItemCommand  : undefined),
                        tdRemoveItemCommand: scope.options && scope.options.tdRemoveItemCommand ? scope.options.tdRemoveItemCommand : (attrs.tdRemoveItemCommand ? attrs.tdRemoveItemCommand  : undefined),
                        tdClearCommand: scope.options && scope.options.tdClearCommand ? scope.options.tdClearCommand : (attrs.tdClearCommand ? attrs.tdClearCommand  : undefined),
                        tdOnUpdateCommand: scope.options && scope.options.tdOnUpdateCommand ? scope.options.tdOnUpdateCommand : (attrs.tdOnUpdateCommand ? attrs.tdOnUpdateCommand  : undefined)
                    };
                    // initialize array if needed
                    ngModel.$viewValue = ngModel.$viewValue ? ngModel.$viewValue : [];
                    // initialize register service
                    var register = tdEventRegisterService(element);
                    // define search function
                    scope.__search = function(val){
                        // return the result of the promise or array
                        return $q.when(scope[attrs.tdGetterFunction](val)).then(
                            // on success
                            function(data){
                                // result
                                var result = [];
                                // if we have any filter
                                if(angular.isUndefined(tAttr.tdClientFilter) || tAttr.clientFilter === true){
                                    // iterate
                                    for(var i = 0; i< data.length; i++){
                                        // has item representation
                                        var has = false;
                                        // valdiate if we have something in hte model
                                        if(ngModel.$viewValue) {
                                            // validate we already have this items or not
                                            for (var j = 0; j < ngModel.$viewValue.length; j++) {
                                                // validate
                                                if (data[i][attrs.tdDataField] === ngModel.$viewValue[j][attrs.tdDataField]) {
                                                    has = true
                                                }
                                            }
                                        }
                                        // validate if we not have the item in the model
                                        if(!has)
                                            // show the item
                                            result.push(data[i]);
                                    }
                                }
                                else
                                    result = data;
                                // return result
                                return result;
                            }
                        )
                    };
                    // TODO implement delete of elemens on key down
                    // tdRemoveItemCommand
                    scope.removeItem = function(item, i){
                        // validate we are enabled
                        if(!scope._disable) {
                            // set model as touched
                            ngModel.$setTouched();
                            // remove item
                            scope._selectedItems.splice(i, 1);
                            // set view value
                            ngModel.$setViewValue(angular.copy(scope._selectedItems), true);
                            // tell evyone mode changed
                            scope.$emit(commands.tdOnUpdateCommand ? commands.tdOnUpdateCommand : api.tdMultiSelect.tdModelUpdated, ngModel.$viewValue);
                        }
                    };
                    // watch changes in the selection
                    register.listen(scope.$watch('_selectedItem', function(value){
                        // validate values is an object
                        if(angular.isObject(value)) {
                            // add new item to selected items
                            scope._selectedItems.push(value);
                            // set view value
                            ngModel.$setViewValue(angular.copy(scope._selectedItems), true);
                            // empty selected
                            scope._selectedItem = undefined;
                            // set model as touched
                            ngModel.$setTouched();
                            // tell evyone mode changed
                            scope.$emit(commands.tdOnUpdateCommand ? commands.tdOnUpdateCommand : api.tdMultiSelect.tdModelUpdated, ngModel.$viewValue);
                        }
                    }));
                    // listen to update event
                    register.listen(scope.$on(commands.tdAddItemCommand ? commands.tdAddItemCommand : api.tdMultiSelect.tdAddItemCommand, function(e, value){
                        // stop event propagation
                        register.cancelPropagation(e);
                        // validate values is an object
                        if(angular.isObject(value)) {
                            // add new item to selected items
                            scope._selectedItems.push(value);
                            // set view value
                            ngModel.$setViewValue(angular.copy(scope._selectedItems), true);
                            // empty selected
                            scope._selectedItem = undefined;
                            // set model as touched
                            ngModel.$setTouched();
                            // tell evyone mode changed
                            scope.$emit(commands.tdOnUpdateCommand ? commands.tdOnUpdateCommand : api.tdMultiSelect.tdModelUpdated, ngModel.$viewValue);
                        }
                    }));
                    // listen to update event
                    register.listen(scope.$on(commands.tdRemoveItemCommand ? commands.tdRemoveItemCommand : api.tdMultiSelect.tdRemoveItemCommand, function(e){
                        // stop event propagation
                        register.cancelPropagation(e);
                        // set model as touched
                        ngModel.$setTouched();
                    }));
                    // listen to update event
                    register.listen(scope.$on(commands.tdClearCommand ? commands.tdClearCommand : api.tdMultiSelect.tdClearCommand, function(e){
                        // stop event propagation
                        register.cancelPropagation(e);
                        // set view value
                        ngModel.$setViewValue([], true);
                        // add new item to selected items
                        scope._selectedItems = [];
                        // empty selected
                        scope._selectedItem = undefined;
                    }));
                    // render function
                    ngModel.$render = function(){
                        // validate values is an object
                        if(ngModel.$viewValue && angular.isArray(ngModel.$viewValue)) {
                            // set value
                            scope._selectedItems = angular.copy(ngModel.$viewValue);
                            // set model as touched
                            ngModel.$setTouched();
                        }
                    };
                    // watch changes in the selection
                    register.listen(scope.$watch(
                        function(){
                            // watch on changes in the disabled state need to be evaluates each time
                            return element.attr('disabled');
                        }, function(value){
                            // validate
                            scope._disable = (angular.isString(value) && (value === 'disabled'));
                        }
                    ));
                };
            }
        }
    }]);
});