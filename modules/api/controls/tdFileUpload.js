/**
 * Created by guiyep on 10/09/15.
 */
define(['../api', 'angularFileUpload'], function (api) {
    'use strict';

    api.tdFileUpload = {
      tdClearCommand: 'tdFileUpload-tdClearCommand'
    };

    api.directive('tdFileUpload', ['tdEventRegisterService', function (tdEventRegisterService) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            templateUrl: 'modules/api/directives/templates/fileUpload.html',

            controller: [ '$scope', 'FileUploader', '$log', function($scope, FileUploader, $log){
                // initialize fileOptions
                $scope.fileOptions = {
                    url: '../Modules/Api/upload.php',
                    ui: {
                        showQueue : true,
                        showUpdateAll: true,
                        showCancelAll: true,
                        showRemoveAll: true,
                        showMultiple : true,
                        showSingle: false,
                        queueLength : 10,
                        showThumb: true
                    },
                    commands: {
                        onWhenAddingFileFailed: '',
                        onAfterAddingFile: '',
                        onAfterAddingAll: '',
                        onBeforeUploadItem: '',
                        onProgressItem: '',
                        onProgressAll : '',
                        onSuccessItem: '',
                        onErrorItem: '',
                        onCancelItem: '',
                        onCompleteItem: '',
                        onCompleteAll: ''
                    },
                    filters: [],
                    removeAfterUpload: false
                };
                // extend with pre defined optiosn
                angular.extend($scope.fileOptions, $scope.options || {});
                // define uploader and save it in the scope
                var uploader = new FileUploader($scope.fileOptions);
                // validate is html
                if(uploader.isHTML5){
                    // initialize uploader
                    $scope.uploader = uploader;
                    // FILTERS
                    uploader.filters.push({
                        name: 'customFilter',
                        fn: function() { // item, options
                            return this.queue.length < $scope.fileOptions.ui.queueLength;
                        }
                    });
                    // merge filters if any
                    $.merge(uploader.filters, $scope.fileOptions.filters);
                    // validate if we have commands
                    if($scope.fileOptions.commands) {
                        // validate particular command
                        if($scope.fileOptions.commands.onWhenAddingFileFailed) {
                            // add command
                            uploader.onWhenAddingFileFailed = function (item, filter, options) {
                                // emit event upward
                                $scope.$emit($scope.fileOptions.commands.onWhenAddingFileFailed, item, filter, options);
                            };
                        }
                        // validate particular command
                        if($scope.fileOptions.commands.onAfterAddingFile) {
                            // add command
                            uploader.onAfterAddingFile = function (fileItem) {
                                // emit event upward
                                $scope.$emit($scope.fileOptions.commands.onAfterAddingFile, fileItem);
                            };
                        }
                        // validate particular command
                        if($scope.fileOptions.commands.onAfterAddingAll) {
                            // add command
                            uploader.onAfterAddingAll = function (addedFileItems) {
                                // emit event upward
                                $scope.$emit($scope.fileOptions.commands.onAfterAddingAll, addedFileItems);
                            };
                        }
                        // validate particular command
                        if($scope.fileOptions.commands.onBeforeUploadItem) {
                            // add command
                            uploader.onBeforeUploadItem = function (item) {
                                // emit event upward
                                $scope.$emit($scope.fileOptions.commands.onBeforeUploadItem, item);
                            };
                        }
                        // validate particular command
                        if($scope.fileOptions.commands.onProgressItem) {
                            // add command
                            uploader.onProgressItem = function (fileItem, progress) {
                                // emit event upward
                                $scope.$emit($scope.fileOptions.commands.onProgressItem, fileItem, progress);
                            };
                        }
                        // validate particular command
                        if($scope.fileOptions.commands.onProgressAll) {
                            // add command
                            uploader.onProgressAll = function (progress) {
                                // emit event upward
                                $scope.$emit($scope.fileOptions.commands.onProgressAll, progress);
                            };
                        }
                        // validate particular command
                        if($scope.fileOptions.commands.onSuccessItem) {
                            // add command
                            uploader.onSuccessItem = function (fileItem, response, status, headers) {
                                // emit event upward
                                $scope.$emit($scope.fileOptions.commands.onSuccessItem, fileItem, response, status, headers);
                            };
                        }
                        // validate particular command
                        if($scope.fileOptions.commands.onWhenAddingFileFailed) {
                            // add command
                            uploader.onWhenAddingFileFailed = function (fileItem, response, status, headers) {
                                // emit event upward
                                $scope.$emit($scope.fileOptions.commands.onWhenAddingFileFailed, fileItem, response, status, headers);
                            };
                        }
                        // validate particular command
                        if($scope.fileOptions.commands.onCancelItem) {
                            // add command
                            uploader.onCancelItem = function (fileItem, response, status, headers) {
                                // emit event upward
                                $scope.$emit($scope.fileOptions.commands.onCancelItem, fileItem, response, status, headers);
                            };
                        }
                        // validate particular command
                        if($scope.fileOptions.commands.onCompleteItem) {
                            // add command
                            uploader.onCompleteItem = function (fileItem, response, status, headers) {
                                // emit event upward
                                $scope.$emit($scope.fileOptions.commands.onCompleteItem, fileItem, response, status, headers);
                            };
                        }
                        // validate particular command
                        if($scope.fileOptions.commands.onCompleteAll) {
                            // add command
                            uploader.onCompleteAll = function () {
                                // emit event upward
                                $scope.$emit($scope.fileOptions.commands.onCompleteAll);
                            };
                        }
                    }
                    // add command
                    uploader.onErrorItem = function(item, response, status, headers) {
                        // TODO display error
                    };
                }
                else{
                    // log error
                    $log.error('you must use a browser that support HTML5');
                    // cancel all
                    uploader.cancelAll();
                    // destroy uploader
                    uploader.destroy();
                }
            }],

            link: function(scope, element){
                // listen to destroy event
                var register = tdEventRegisterService(element);
                // tdClearCommand event
                register.listen(scope.$on(scope.fileOptions.commands.tdClearCommand? scope.fileOptions.commands.tdClearCommand : api.tdFileUpload.tdClearCommand, function(e){
                    // cancel event propagation
                    register.cancelPropagation(e);
                    // remove all items
                    scope.uploader.cancelAll();
                    // remove all
                    scope.uploader.clearQueue();
                }));
                // on destroy
                register.destroy(function(){
                    // validate we have a uploader
                    if(scope.uploader){
                        // cancel all
                        scope.uploader.cancelAll();
                        // destroy uploader
                        scope.uploader.destroy();
                        // release memory
                        scope.uploader = undefined;
                    }
                });
            }
        }
    }]);
});