
/**
 * Created by guiyep on 31/08/15.
 */

define(['../api'], function (api) {
    'use strict';

    api.tdAutocomplete = {
        tdModelUpdated : 'td-autocomplete-select-model-updated',
        tdEditValueContent: 'td-autocomplete-edit-value'
    };

    api.directive('tdAutocomplete', ['tdEventRegisterService', '$log', function (tdEventRegisterService, $log) {
        return {
            // restrict to attribute or element or class name
            restrict: 'C',

            require: 'ngModel',

            template: '<input class="form-control td-multi-select-input" ng-disabled="_disable" ng-model="_selectedItem" type="text">',

            compile: function(tElement, tAttr){
                // validate we have all set
                if(tAttr.tdValueField && tAttr.tdDataField && tAttr.tdGetterFunction) {
                    // get input
                    var input = $(tElement).find('.td-multi-select-input');
                    // set typeahead
                    input.attr('typeahead', 'obj as obj.' + tAttr.tdValueField + ' for obj in ' + tAttr.tdGetterFunction + '($viewValue)');
                }
                else{
                    $log.error('need attrs to be set')
                }
                // return link function
                return function(scope, element, attrs, ngModel){
                    // define commandas
                    var commands = {
                        tdOnUpdateCommand: scope.options && scope.options.tdOnUpdateCommand ? scope.options.tdOnUpdateCommand : (attrs.tdOnUpdateCommand ? attrs.tdOnUpdateCommand  : undefined),
                        tdEditValueContent: scope.options && scope.options.tdEditValueContent ? scope.options.tdEditValueContent : (attrs.tdEditValueContent ? attrs.tdEditValueContent  : undefined)
                    };
                    // initialize register service
                    var register = tdEventRegisterService(element);
                    // watch changes in the selection
                    register.listen(scope.$watch('_selectedItem', function(value){
                        // validate values is an object
                        if (angular.isObject(value)) {
                            // set view value
                            ngModel.$setViewValue(angular.copy(scope._selectedItem), true);
                            // set model as touched
                            ngModel.$setTouched();
                            // tell evyone mode changed
                            scope.$emit(commands.tdOnUpdateCommand ? commands.tdOnUpdateCommand : api.tdAutocomplete.tdModelUpdated, ngModel.$viewValue);
                        }
                        else {
                            // set view value
                            ngModel.$setViewValue(null, true);
                        }
                    }));
                    // listen to edit event, for the case that we have to edit the content of the object
                    register.listen(scope.$on(commands.tdEditValueContent ? commands.tdEditValueContent : api.tdAutocomplete.tdEditValueContent, function(e, value){
                        // stop event propagation
                        register.cancelPropagation(e);
                        // validate values is an object
                        if(angular.isObject(value)) {
                            // set value
                            scope._selectedItem = value;
                            // set view value
                            ngModel.$setViewValue(angular.copy(value), true);
                            // set model as touched
                            ngModel.$setTouched();
                        }
                        else{
                            // set view value
                            ngModel.$setViewValue(null, true);
                        }
                    }));
                    // render function
                    ngModel.$render = function(){
                        // validate values is an object
                        if(angular.isObject(ngModel.$viewValue)) {
                            // set value
                            scope._selectedItem = angular.copy(ngModel.$viewValue);
                            // set model as touched
                            ngModel.$setTouched();
                        }
                    };
                    // watch changes in the selection
                    register.listen(scope.$watch(
                        function(){
                            // watch on changes in the disabled state need to be evaluates each time
                            return element.attr('disabled');
                        }, function(value){
                            // validate
                            scope._disable = (angular.isString(value) && (value === 'disabled'));
                        }
                    ));
                };
            }
        }
    }]);
});