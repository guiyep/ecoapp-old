/**
 * Created by Guille on 6/12/2014.
 */
define(['../api'], function (api) {
    'use strict';

    api.tdGoogleMap = {
        setMarkers : 'td-google-map-set-markers',
        resetMarkers: 'td-google-map-reset-markers',
        displayRoute: 'td-google-map-set-route',
        reload: 'td-google-map-reload',
        loaded: 'td-google-map-loaded'
    };

    // tdGoogleMap
    api.directive('tdGoogleMap', ['tdEventRegisterService','tdUniqueIdGenerator', 'tdGoogleMapJqueryLoader', '$q', '$compile',
        function (tdEventRegisterService,  tdUniqueIdGenerator, tdGoogleMapJqueryLoader, $q, $compile) {
            return {
                // restrict to attribute or element or class name
                restrict: 'AEC',
                // link function
                compile: function (tElement) {
                    // get unique contianer id
                    var uniqueId = tdUniqueIdGenerator();
                    // define container element
                    var container = angular.element('<div id="'+uniqueId+ '" class="td-loading td-google-map-container"></div>');
                    // apend element
                    tElement.append(container);
                    // set return function
                    return function (scope, element) {
                        // get map container
                        var mapContainer = $(element).find('.td-google-map-container');
                        // initialize register service
                        var register = tdEventRegisterService(element);
                        // get google reference
                        var gOptions = {containerId : uniqueId, options: scope.options};
                        // initialize
                        var init = false;
                        // set width and height
                        if(scope.options.width && scope.options.height){
                            // wait until render of the div
                            register.afterRender(function(){
                                // set css, the width and height need to be set to have the google map working
                                jQuery(mapContainer).css({
                                    width: scope.options.width === 'parent' ? $(element).parent().width(): scope.options.width,
                                    height: scope.options.height === 'parent' ? $(element).parent().width() : scope.options.height
                                });
                                // set css
                                jQuery(element).css({
                                    width: scope.options.width === 'parent' ? $(element).parent().width() : scope.options.width,
                                    height: scope.options.height === 'parent' ? $(element).parent().height() : scope.options.height
                                });
                            });
                        }
                        // wait until all is rendered
                        register.afterRender(function(){
                            // if we have loading indicator enabled
                            if(scope.options.loading)
                                scope.$emit(scope.options.loading.start ? scope.options.loading.start : api.loadingEvents.start, uniqueId)
                        });
                        // if was not already initialized
                        if(!tdGoogleMapJqueryLoader.isLoaded()) {
                            // load google map jquery plugin
                            tdGoogleMapJqueryLoader.load();
                            // initialize google plugin
                            gOptions = tdGoogleMapJqueryLoader.trigger('googleMapPluginInitialize')(gOptions);
                        }
                        else{
                            // if it is already initialize again
                            gOptions = tdGoogleMapJqueryLoader.trigger('googleMapInitialize')(gOptions);
                        }
                        // wait until renderer is resolve to emit event upwad to let know the app that the map is loaded
                        gOptions.rendered.then(
                            // success
                            function(){
                                // emit event upwards
                                scope.$emit(api.tdGoogleMap.loaded);
                                // if we have loading indicator enabled
                                if(scope.options.loading)
                                    scope.$emit(scope.options.loading.end ? scope.options.loading.end : api.loadingEvents.end, uniqueId)
                            }
                        );
                        // parse latLong function
                        var parseLatLong = function(latlng){
                            // defer execution
                            var deferred = $q.defer();
                            // get google map geocoder
                            var geocoder = new google.maps.Geocoder;
                            // use geocoder
                            geocoder.geocode({'location': latlng}, function(results, status) {
                                // validate status ok of the response of the api
                                if (status === google.maps.GeocoderStatus.OK) {
                                    // validate response
                                    if (results && results.length > 0) {
                                        // reject promise
                                        deferred.resolve(results)
                                    } else {
                                        // reject promise
                                        deferred.reject()
                                    }
                                } else {
                                    // reject promise
                                    deferred.reject()
                                }
                            });
                            // return promise
                            return deferred.promise;
                        };
                        // check if we have any event to add
                        if(scope.options.eventCommands){
                            // function
                            var execCommand = function(command){
                                // return function
                                return function(e){
                                    // parse event
                                    parseLatLong({ lat:  e.latLng.lat() , lng: e.latLng.lng()}).then(
                                        // on success emit event
                                        function(data){
                                            // emit event upwards
                                            scope.$emit(command);
                                        }
                                    )
                                }
                            };
                            // iterate
                            $.each(scope.options.eventCommands, function(i, e){
                                // add event to google map
                                tdGoogleMapJqueryLoader.trigger('addEvent')(gOptions, e.name , execCommand(e.command))
                            });
                        }
                        // check if we have any event to add
                        if(angular.isArray(scope.options.menu)){
                            // define current event
                            var _cureentEvent;
                            // define click function
                            scope._itemClick = function(item){
                                // parse event
                                parseLatLong({ lat:  _cureentEvent.latLng.lat() , lng: _cureentEvent.latLng.lng()}).then(
                                    // on success emit event
                                    function(data){
                                        // emit event
                                        scope.$emit(item.command, data);
                                        // hide menu
                                        $(menu).hide();
                                    }
                                )
                            };
                            // create context menu
                            var menu = angular.element('<div id="context_'+uniqueId+'" class="td-google-map-context-menu">' +
                                '<div ng-repeat="item in options.menu" ng-click="_itemClick(item)">' +
                                    '<a>{{item.name}}</a>' +
                                '</div>' +
                            '</div>');
                            // hide it
                            $('body').append(menu);
                            // compile it
                            $compile(menu)(scope);
                            // hide menu
                            $(menu).hide()
                            // add event to google map
                            tdGoogleMapJqueryLoader.trigger('addEvent')(gOptions, 'rightclick' , function(e){
                                // save current event
                                _cureentEvent = e;
                                // clicked
                                $(menu).show();
                                // set position
                                $(menu).offset({ left: e.pixel.x, top: e.pixel.y });
                            });
                            // add event to google map
                            tdGoogleMapJqueryLoader.trigger('addEvent')(gOptions, 'click' , function(e){
                                // clicked
                                $(menu).hide();
                            });
                            // TODO add mobile events
                        }
                        // on destroy
                        register.destroy(function(){
                            // remove listeners
                            tdGoogleMapJqueryLoader.trigger('destroyGoogleMapsListeners')(gOptions);
                        });                    // on set marks
                        register.listen(scope.$on(api.tdGoogleMap.setMarkers, function(ev, markers, conf){
                            // stop propagation
                            register.cancelPropagation(ev);
                            // remove markers
                            tdGoogleMapJqueryLoader.trigger('destroyMarkers')(gOptions);
                            // load markers
                            tdGoogleMapJqueryLoader.trigger('loadMarkers')(markers, gOptions, conf);
                        }));
                        // on set reset markers
                        register.listen(scope.$on(api.tdGoogleMap.displayRoute, function(ev, markers, op){
                            // stop propagation
                            register.cancelPropagation(ev);
                        }));
                        // on set reset markers
                        register.listen(scope.$on(api.tdGoogleMap.reload, function(ev){
                            // stop propagation
                            register.cancelPropagation(ev);
                            // remove markers
                            tdGoogleMapJqueryLoader.trigger('destroyMarkers')(gOptions);
                            // load markers
                            tdGoogleMapJqueryLoader.trigger('loadMarkers')(gMap, undefined, gOptions);
                        }));
                        // on reset markers
                        register.listen(scope.$on(api.tdGoogleMap.resetMarkers, function(ev){
                            // stop propagation
                            register.cancelPropagation(ev);
                            // remove markers
                            tdGoogleMapJqueryLoader.trigger('destroyMarkers')(gOptions);
                        }));
                    }
                }
            };
        }]);
});