/**
 * Created by Guille on 23/01/2015.
 */
define(['../api'], function (api) {
    'use strict';

    api.tdForm = {
        finish: 'td-form-finish'
    };

    api.directive('tdForm', [ 'tdPageLoaderService','$parse', '$log', '$compile', function ( tdPageLoaderService, $parse, $log, $compile) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            require: 'ngModel',

            link: function(scope, element, attrs){
                // check we have a formName defined
                if(attrs && attrs.formName){
                    // add finish label
                    var finishLabel = attrs.finishLabel;
                    // set entity
                    scope.entity = attrs.formName;
                    // extend model
                    angular.extend(scope, $parse(attrs.ngModel)(scope));
                    // validate we have already cached
                    tdPageLoaderService.form(attrs.formName, scope, element, attrs.controller).then(
                        // on success
                        function(){
                            // append the button to the end
                            var form = $(element).find('[name="'+ attrs.formName +'"]');
                            // validate that is a valid form
                            if(form.length !== 0 && attrs.disableFinish != true){
                                // current scope
                                var cScope = angular.element(form).scope();
                                // define submit
                                cScope._submit = function(){
                                    // emit event with the model
                                    scope.$emit(api.tdForm.finish, $parse(attrs.ngModel)(cScope));
                                };
                                // define finish button TODO remove class
                                var button = angular.element('<div class="request-row"><button type="button" ng-click="_submit()" ng-disabled="!'+ attrs.formName +'.$valid" class="btn btn-info" td-i18n>' + (finishLabel ? finishLabel : '{ Finish  | FINISH }') +'</button></div>');
                                // append submit button
                                form.append(button);
                                // compile it
                                $compile(button)(cScope);
                            }
                            else{
                                $log.error('we have an invalid form');
                            }
                        }
                    );
                }
            }
        }
    }]);
});