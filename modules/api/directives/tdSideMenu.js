/**
 * Created by Guille on 07/10/2014.
 */
define(['../api', 'jQuery'], function (api, $) {
    'use strict';

    api.sideMenuEvents = {
        menuOpened: 'side-menu-opened'
    };

    api.directive('tdSideMenu', ['tdDeviceParameterService', function(tdDeviceParameterService) {
        return {
            // restrict to element or class name
            restrict: 'E',

            template:
                '<div class="side-menu-bar side-menu-bar-if" style="width:{{internal.width}}px" ng-if="visible">' +
                        '<div class="side-menu-bar-space">' +
                            '<div class="side-menu-bar-button" ng-click="expand()">' +
                                '<i ng-class="internal.expandIconClass" ></i>' +
                            '</div>' +
                        '</div>' +
                        '<div ng-repeat="item in options.list" ng-class="internal.expandListClass">' +
                            '<div id="{{item.id}}">' +
                                '<i ng-class="item.class" ng-if="item.class"></i>' +
                                '<label class="side-menu-bar-label" ng-if="item.text" >{{item.text}}</label>' +
                                '<a ng-if="item.childs" ng-click="openSubMenu(item)">{{item.label}}</a>' +
                                '<a ng-if="!item.childs" href="{{item.url}}" ng-click="openingMenu(item)">{{item.label}}</a>' +
                            '</div>' +
                        '</div>' +
                '</div>',

            controller: [ '$scope', function($scope){
                // update visibility on container hover
                if($scope.options && $scope.options.sourceId && $scope.options.containerId && $scope.options.width){
                    // internal parameters service initialization (this will set the parameters based on the device used)
                    $scope.expanded = tdDeviceParameterService('tdSideMenu', 'expanded', false );
                    // initialize width
                    $scope.internal = { width : $scope.options.width, expandIconClass: tdDeviceParameterService('tdSideMenu', 'expand', 'side-menu-bar-space-expand' ) };
                    // get opener
                    var container = $('#' + $scope.options.containerId) || undefined;
                    // get opener
                    var element = $('#' + $scope.options.sourceId) || undefined;
                    // define openingMenu
                    $scope.openingMenu = function(item){
                        // emit event
                        $scope.$emit( api.sideMenuEvents.menuOpened, item);
                    };
                    // define expand
                    $scope.expand = function(){
                        // save state
                        $scope.expanded = !$scope.expanded;
                        // set width
                        $scope.internal.width = ($scope.expanded ? $scope.options.width + container.width() : $scope.options.width);
                        // set collapsed class
                        $scope.internal.expandIconClass = !$scope.expanded ? 'side-menu-bar-space-expand' : 'side-menu-bar-space-collapse';
                        // set as expand list
                        $scope.internal.expandListClass = $scope.expanded ?'side-menu-bar-list-expand' : '';
                    };
                    // validate we have an element
                    if(element && container){
                        // set hand cursor
                        element.css({cursor:'pointer'});
                        // click event
                        element.click(
                            // on click event functionality change visibility
                            function(){
                                // inside apply update visibility
                                $scope.$apply(function(){
                                    // update visibility
                                    $scope.visible = !$scope.visible;
                                    // remove sub menu
                                    $scope.submenu = undefined;
                                    // set width
                                    container.css({width: container.width() - ($scope.visible ? $scope.options.width : -  $scope.options.width)});
                                });
                            }
                        )
                    }
                    // validate we have an element
                    if(container) {
                        // set left
                        container.css({float:'left'});
                    }
                }

            }]
        }
    }]);
});


