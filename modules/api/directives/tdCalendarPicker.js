/**
 * Created by Guille on 07/01/2015.
 */
define(['../api', 'moment'], function (api, moment) {
    'use strict';

    api.directive('tdCalendarPicker', ['$log','$timeout', function ($log, $timeout) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            require: 'ngModel',

            template: '<div class="td-calendar-datepicker"></div>' ,

            compile: function() {
                // return link function
                return function(scope, element, attrs, ngModel){
                    // bind selection function
                    var bindSelections = function(year, month){
                        $timeout(function(){
                            // set selections
                            if(scope.options && scope.options.selection) {
                                // get days
                                var days = dateElement.find('td a');
                                // iterate in days
                                $.each(days, function (idx, dayElement) {
                                    // get current moment
                                    var currentMoment = moment().date($(dayElement).text()).year(year).month(month);
                                    // iterate in selection items
                                    for (var i = 0; i < scope.options.selection.length; i++) {
                                        // get item
                                        var selection = scope.options.selection[i];
                                        // set color
                                        selection._color = !selection._color ? '#'+Math.floor(Math.random()*16777215).toString(16) : selection._color;
                                        // validate
                                        if (selection.from && selection.to && selection.label) {
                                            // selected.from, to, label, color
                                            var mFrom = moment(selection.from), mTo = moment(selection.to).add(1, 'day');
                                            // validate if we have to set color
                                            if (currentMoment.isBefore(mTo) && currentMoment.isAfter(mFrom)) {
                                                // set color and label
                                                var identifier = angular.element('<div class="td-calendar-selected" title="'+selection.label+'"></div>');
                                                // set css
                                                identifier.css('background-color', selection._color);
                                                // append element
                                                $(dayElement).parent().append(identifier);
                                            }
                                        }
                                    }

                                });
                            }
                        }, 0);
                    };
                    // define initial options
                    var dateOptions = {
                        // on select function
                        onSelect: function(date){
                            var mDate = moment(date);
                            // apply changes
                            scope.$apply(function(){
                                // save date selected
                                ngModel.$setViewValue(mDate.toDate());
                            });
                            // bind selection
                            bindSelections(mDate.year(), mDate.month());
                        },
                        onChangeMonthYear: function(year, month){
                            // bind selection
                            bindSelections(year, month - 1);
                        }
                    };
                    // extend options
                    if(scope.options){
                        //extend options from controller
                        angular.extend(dateOptions, scope.options);
                    }
                    var dateElement = $(element).children('.td-calendar-datepicker');
                    // create date picker
                    dateElement.datepicker(dateOptions);
                    // set render function
                    ngModel.$render = function() {
                        // get moment month and year
                        var currentMoment = moment(dateElement.find(".ui-datepicker-title").text());
                        // bind function
                        bindSelections(currentMoment.year(), currentMoment.month());
                        // set date (ensure we have a date in the model)
                        dateElement.datepicker( "setDate", moment(ngModel.$viewValue).toDate() );
                    }
                }
            }
        };
}]);
});