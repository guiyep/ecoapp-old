/**
 * Created by guiyep on 23/06/15.
 */
define(['../api'], function (api) {
    'use strict';

    api.directive('tdToolbarItemCustom', ['$compile', function ($compile) {
        return {
            // define link
            link: function(scope, element){
                // append directive and compile if any
                if(scope.options.directive){
                    // define element
                    var el = angular.element('<div class="'+scope.options.directive+'"></div>');
                    // append element
                    element.append(el);
                    // compile it
                    $compile(scope)(el);
                }
            }
        }
    }]);
});