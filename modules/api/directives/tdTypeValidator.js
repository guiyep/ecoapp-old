/**
 * Created by Guille on 21/11/2014.
 */
define(['../api'], function (api) {
    'use strict';

    // tdRequireField
    api.directive('tdTypeValidator', ['$log','$compile','tdEventRegisterService', function ($log, $compile, tdEventRegisterService) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',
            // we need the ngModel to ue this directive
            require: 'ngModel',
            // link function
            // set return function
            link: function (scope, element, attrs, ngModel ) {
                // find closes ngForm
                var form = jQuery(element).closest('.ng-form');
                // get form name
                var name = !form ? undefined : form.attr('name');
                // validate we have a all the needed elements defined
                if(!!name && attrs.typeValidator){
                    // create element
                    var text = angular.element(
                            '<div class="require-field-text" ng-if="'+name+'.$error.' + (attrs.expression ? 'expression' : element.attr('type')) + '" >' +
                                '<label td-i18n>'+attrs.typeValidator+'</label>' +
                            '</div>'
                    );
                    // prepend element
                    jQuery(text).insertAfter(element);
                    // compile
                    $compile(text)(scope);
                    // check if we have any expression to validate
                    if(attrs.expression) {
                        // initialize register service
                        var register = tdEventRegisterService(element);
                        // set function
                        var validator = function () {
                            // check if we have a false value
                            if (!scope.$eval(attrs.expression)) {
                                // it is invalid, return undefined (no model update)
                                ngModel.$setValidity('expression', false);
                                // return undefined, is invalid
                                return undefined;
                            }
                            // it is valid, return viewValue (no model update)
                            ngModel.$setValidity('expression', true);
                            // is valid
                            return ngModel.$viewValue;
                        };
                        // watch changes in the expression
                        register.listen( scope.$watch(attrs.expression, validator));
                        // execute validator
                        validator(ngModel.$viewValue);
                        // create validator in model
                        ngModel.$parsers.unshift(validator);
                    }
                }
                else {
                    $log.error('the type validator need a form to be set with a name and a text');
                }
            }
        };
    }]);
});