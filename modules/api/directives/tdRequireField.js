/**
 * Created by Guille on 19/11/2014.
 */
define(['../api'], function (api) {
    'use strict';

    // tdRequireField (This directive work in conjuntion with tdTypeValidator)
    api.directive('tdRequireField', ['$log', '$compile', 'tdUniqueIdGenerator', '$timeout', function ($log, $compile, tdUniqueIdGenerator, $timeout) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',
            // we need the ngModel to ue this directive
            require: 'ngModel',
            // link function
            compile: function(tElements, tAttrs) {
                // find closes ngForm
                var form = $(tElements).closest('.ng-form');
                // get form name
                var formName = !form ? undefined : form.attr('name');
                // get field name
                var fieldName = tAttrs.name ? tAttrs.name : tdUniqueIdGenerator(true);
                // set field name if we do not have any
                if (!tAttrs.name) {
                    // set field name
                    $(tElements).attr('name', fieldName);
                }
                // return link function
                return function (scope, element, attrs, ngModel) {
                    // validate we have a form set
                    if (!!formName && fieldName) {
                        // create req element <i class="fa fa-exclamation"></i>
                        var req = angular.element('<i class="require-field-required fa fa-exclamation" ng-if="' + formName + '.' + fieldName + '.$error.requiredField"></i>');
                        // insert before element
                        $(req).insertBefore(element);
                        // compile
                        $compile(req)(scope);
                        // check text if we have any text
                        if (attrs.tdRequireField && !attrs.tdTypeValidator) {
                            // append text
                            var text = angular.element(
                                '<div class="require-field-text" ng-if="' + formName + '.' + fieldName + '.$error.requiredField && ' + formName + '.' + fieldName + '.$touched">' +
                                    '<label td-i18n>' + attrs.tdRequireField + '</label>' +
                                '</div>'
                            );
                            // insert after element
                            jQuery(text).insertAfter(element);
                            // compile
                            $compile(text)(scope);
                        }
                        // set function
                        var validator = function (viewValue) {
                            // check if is empty
                            if ((angular.isUndefined(viewValue) || (ngModel.$isEmpty(viewValue)) && (attrs.on ? scope.$eval(attrs.on) : true)) || (angular.isArray(viewValue) && viewValue.length === 0)) {
                                // it is invalid, return undefined (no model update)
                                ngModel.$setValidity('requiredField', false);
                                // return undefined, is invalid
                                return undefined;
                            }
                            // it is valid, return viewValue (no model update)
                            ngModel.$setValidity('requiredField', true);
                            // is valid
                            return viewValue;
                        };
                        $timeout(function(){
                            // execute validator
                            validator(ngModel.$viewValue);
                        }, 0);
                        // create validator in model
                        ngModel.$parsers.unshift(validator);
                    }
                    else {
                        $log.error('the require field need a form to be set with a name and a text');
                    }
                }
            }
        };
    }]);
});