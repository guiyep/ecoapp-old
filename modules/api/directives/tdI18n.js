/**
 * Created by Guille on 14/11/2014.
 */
define(['./../api'], function (api) {
    'use strict';

    // i18nLabel
    api.directive('tdI18n', [ 'suitResourcesService', '$parse', function (suitResourcesService, $parse) {
            return {
                restrict: 'A',
                // link function Parameters: { default-text | resource-name | bind(optional)}
                link: function(scope, element){
                    // get text
                    var text = element.text();
                    // get trim text
                    var trimText = text.trim();
                    // validate string
                    if (trimText.indexOf('{') == 0 && trimText.indexOf('}') == (trimText.length - 1)){
                        // remove indicators
                        var a = text.replace("{","");
                        var b = a.replace("}","");
                        // split text
                        var arr = b.split("|");
                        // validate we do not have any bind
                        if(!arr[2] || (arr[2] && !angular.isDefined($parse(arr[2].trim())(scope)))){
                            // set text
                            element.text(suitResourcesService.i18n.translate(arr.length > 1 ? arr[1].trim() : "" ,arr[0]));
                        }
                        else{
                            // value of the bind, this will be only evaluated one time
                            var value = $parse(arr[2].trim())(scope);
                            // set text
                            element.text(suitResourcesService.i18n.translate(value));
                        }
                    }
                }
            };
        }]);
});