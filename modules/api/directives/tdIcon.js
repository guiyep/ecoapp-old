/**
 * Created by Guille on 23/01/2015.
 */
define(['../api'], function (api) {
    'use strict';

    api.directive('tdIcon', ['suitResourcesService', '$log','$location', function (suitResourcesService, $log, $location) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            priority: '700',

            link: function(scope, element, attrs){
                // validate type
                if(attrs.type) {
                    // add class
                    element.addClass('td-icon-content');
                    // validate we have any type set
                    if(attrs.type || attrs.cssClass) {
                        // append icon
                        element.append(angular.element('<i ' + (suitResourcesService.web.data("ICON")['type'][attrs.type] ? (' class="' + suitResourcesService.web.data("ICON")['type'][attrs.type] + ' '+ (attrs.cssClass ? attrs.cssClass : '') + '"') : (attrs.cssClass ? attrs.cssClass : '')) + '>'));
                    }
                    // validate we have any legend
                    if (attrs.legend) {
                        // define legend element
                        var legend = angular.element('<div class="td-icon-legend ' + (attrs.position ? suitResourcesService.web.data("ICON")['position'][attrs.position] : '') + '" > ' + attrs.legend + '</div>');
                        // append element
                        element.append(legend)
                    }
                    // validate if we need to add hover effect
                    if((attrs.hover && attrs.hover == "true") || attrs.hoverClass){
                        // add hover effect
                        element.addClass( attrs.hoverClass ?  attrs.hoverClass : 'td-icon-content-hover');
                    }
                    // set click event
                    $(element).click(function (e) {
                        // cancel parent event
                        e.stopPropagation();
                        // if we have an event set, do not relay in the attr obj since the event can be manipulated
                        if (angular.isDefined(element.attr('event')) && !attrs.url) {
                            // emit custom event
                            scope.$emit(element.attr('event'), attrs.options ? scope.$eval(attrs.options) : {});
                        }
                        else if (!attrs.event && attrs.url) {
                            // apply changes
                            scope.$apply(function(){
                                // change location on click
                                $location.path(attrs.url)
                            });
                        }
                    });
                }
                else {
                    $log.error('tdIcon needs type');
                }
            }
        }
    }]);
});