/**
 * Created by Guille on 21/01/2015.
 */
define(['../api'], function (api) {
    'use strict';

    api.directive('tdItemExpandableContainer', ['tdEventRegisterService', function (tdEventRegisterService) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            transclude: true,

            template: '<div class="td-item-expandable-container-content" ng-click="open($event)" ng-style="objClass">' +
                    '<ng-transclude></ng-transclude>' +
                '</div>',
            // link function
            controller: ['$scope', '$element','$attrs', function ($scope, $element, $attrs) {
                // register element
                var register = tdEventRegisterService($element);
                // get height from options
                var maxHeight = $scope.options && $scope.options.height ? $scope.options.height : null;
                var minHeight = $($element).height();
                // open function
                var open = function() {
                    // validate how to open
                    if($scope.options){
                        // wait until all is rendered
                        register.afterRender(
                            function(){
                                // we do not have a height?d
                                if(!maxHeight){
                                    // get expander
                                    var expander = $($element).find('.expanded');
                                    // find if we have any to expand
                                    var content = expander.find('ng-transclude:first');
                                    // find expanded element and set value
                                    var expandedElement = (content && (content.children().height() > 0)) ? content.children() : expander;
                                    // set value
                                    maxHeight = expandedElement && expandedElement.length > 0 ? expandedElement.height() : 0;
                                }
                                // this is for internally use
                                $($scope.options.container ? $($element).closest('.' + $scope.options.container) : $element.parent()).height($scope.expanded ? maxHeight : minHeight);
                            }
                        )
                    }
                };
                // open functionality
                $scope.open = function (e) {
                    // validate we use click event
                    if($attrs.click != 'disabled' && ($scope.options && $scope.options.click != 'disabled')) {
                        // cancel propagation
                        register.cancelPropagation(e);
                        // update open state
                        $scope.expanded = !$scope.expanded;
                        // open
                        open();
                    }
                };
                // wait until all is rendered, so all is digested
                register.afterRender(
                    function() {
                        // check if we have an open and close command
                        if($attrs && $attrs.openEvent && $attrs.closeEvent){
                            // define options
                            $scope.options = $scope.options || {};
                            // define opend and close commands
                            $scope.options.commands = {
                                open: $attrs.openEvent,
                                close: $attrs.closeEvent
                            };
                        }
                        // check we have commands set
                        if ($scope.options && $scope.options.commands && $scope.options.commands.open && $scope.options.commands.close) {
                            // listen open event
                            register.listen($scope.$on($scope.options.commands.open.replace('{{', '').replace('}}', ''), function (e) {
                                // cancel propagation
                                register.cancelPropagation(e);
                                // update open state
                                $scope.expanded = true;
                                // open
                                open();
                            }));
                            // listen close event
                            register.listen($scope.$on($scope.options.commands.close.replace('{{', '').replace('}}', ''), function (e) {
                                // cancel propagation
                                register.cancelPropagation(e);
                                // update open state
                                $scope.expanded = false;
                                // open
                                open();
                            }));
                        }
                    }
                )
            }]
        }
    }]);

    api.directive('expanded', [ function () {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            transclude: true,

            template: '<ng-transclude ng-if="expanded"></ng-transclude>'
        }
    }]);

    api.directive('minimized', [ function () {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            transclude: true,

            template: '<ng-transclude ng-if="!expanded"></ng-transclude>'
        }
    }]);
});