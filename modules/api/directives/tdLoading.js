/**
 * Created by Guille on 06/11/2014.
 */
define(['../api', 'jQuery'], function (api, $) {
    'use strict';

    // tdLoading event Name
    api.loadingEvents = {
        start: 'tdLoading-start',
        end: 'tdLoading-end',
        updateLegend: 'tdLoading-update-legend'
    };

    // tdLoading
    api.directive('tdLoading', ['$window','$compile','tdEventRegisterService','tdDeviceParameterService',
        function ($window, $compile, tdEventRegisterService, tdDeviceParameterService) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',
            // link function
            compile: function () {
                // set return function
                return function (scope, element) {
                    // define if was already loaded
                    var loaded = false;
                    // initialize register service
                    var register = tdEventRegisterService(element);
                    // has specific commands
                    var hasLoadingCommands = (scope.options && scope.options.startCommand && scope.options.endCommand);
                    // initialize tdLoading reference
                    var loadingIndicator = !hasLoadingCommands ? $('#tdLoading') : undefined;
                    // find if we already add the element
                    if(!loadingIndicator || loadingIndicator.length == 0){
                        //TODO implement message service to show promotions and more funny facts while is doing something
                        // initialize element
                        loadingIndicator = angular.element(
                            '<div '+(!hasLoadingCommands ? 'id="loadig"' : "") +' class="loading-indicator">' +
                                '<div class="progress">' +
                                    '<div class="fa fa-spinner fa-pulse fa-5x"></div>' +
                                '</div>' +
                                '<div class="progress-legend"></div>' +
                            '</div>');
                        // show indicator
                        jQuery(loadingIndicator[0]).hide();
                        // get element to append
                        var appendElement = $(!hasLoadingCommands ? 'body' : element.parent());
                        // append to document if it is the base, in case we need a separate append to parent
                        appendElement.append(loadingIndicator);
                    }
                    // start event
                    register.listen(scope.$on(api.loadingEvents.start, function(event, id, legend){
                        // stop event propagation
                        register.cancelPropagation(event);
                        // get element by id`
                        var elementRef = $('#' + id);
                        // get attributes
                        jQuery(loadingIndicator[0]).width(id && !tdDeviceParameterService('tdLoading', 'expanded', false) ? elementRef.outerWidth() : $window.screen.width);
                        jQuery(loadingIndicator[0]).height(id && !tdDeviceParameterService('tdLoading','expanded', false)? elementRef.outerHeight(): $window.screen.height);
                        jQuery(loadingIndicator[0]).offset(id && !tdDeviceParameterService('tdLoading','expanded', false)? { top : elementRef.offset().top, left : elementRef.offset().left }: { top : 0, left : 0 } );
                        // set legend if any
                        if(legend)
                            jQuery(loadingIndicator[0]).find('.progress-legend').text(legend);
                        // show indicator
                        jQuery(loadingIndicator[0]).show();
                        // validate we have a element
                        if(elementRef.length > 0 && !loaded && !tdDeviceParameterService('tdLoading','expanded', false)){
                            // get parent scrollabel
                            var parentScoll = loaded = elementRef.scrollParent();
                            // add scroll event
                            parentScoll.scroll(function(){
                                // update offset
                                jQuery(loadingIndicator[0]).offset(id && !tdDeviceParameterService('tdLoading','expanded', false)? { top : elementRef.offset().top, left : elementRef.offset().left }: { top : 0, left : 0 } );
                            });
                        }
                    }));
                    // start event
                    register.listen(scope.$on(api.loadingEvents.updateLegend, function(event, legend){
                        // stop event propagation
                        register.cancelPropagation(event);
                        // set legend if any
                        if(legend)
                            jQuery(loadingIndicator[0]).find('.progress-legend').text(legend);
                    }));
                    // end event
                    register.listen(scope.$on(api.loadingEvents.end , function(event){
                        // remove legend
                        jQuery(loadingIndicator[0]).find('.progress-legend').text('');
                        // hide tdLoading
                        jQuery(loadingIndicator[0]).hide();
                        // stop event propagation
                        register.cancelPropagation(event);
                    }));
                    // wait until destroy
                    register.destroy(function(){
                        // off loaded if any
                        if(loaded  && loaded.length > 0)
                            loaded.off();
                        // hide tdLoading
                        jQuery(loadingIndicator[0]).hide();
                        // get indicator element
                        var indicator = element.parent().find('.tdLoading-indicator');
                        // check if we have any and remove
                        if(indicator && indicator.length > 0)
                            indicator.remove();
                    });
                }
            }
        };
    }]);
});