/**
 * Created by guiyep on 12/08/15.
 */
define(['../api'], function (api) {
    'use strict';

    api.tdMasonaryListEvents = {
        scroll: 'td-masonary-list-scroll'
    };

    api.directive('tdMasonaryList', ['$q', function ($q) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            template: '<div class="td-list" td-init="options = op" ng-style="{ \'width\' : $parent._internal.width + \'px\' , \'height\' : $parent._internal.height + \'px\'}" ng-repeat="op in listOptions"></div>' ,

            controller: ['$scope', '$element', function($scope, $element){
                // get masonary optopns TODO get scrollbar width instead of 20
                $scope._internal = {
                    width : (($($element.parent()).width() / $scope.options.columns) - (20 / 3)),
                    height: $($element).parent().height()
                };
                // extend options
                angular.extend($scope._internal, $scope.options);
                // create options collection from base options
                var options = [];
                // get number of columns
                for(var i = 0; i < $scope._internal.columns; i++){
                    // initialize options for current list
                    options.push(
                        {
                            width: ($scope._internal.width),
                            list: [],
                            pagination: false,
                            templateName: $scope._internal.templateName,
                            selectable: false,
                            scroll: {
                                container: $($element).parent(),
                                type: 'manual'
                            }
                        }
                    )
                }
                // iterator
                var itarator = 0;
                // iterate in list, and add one in each list
                for(var j = 0; j < $scope._internal.list.length; j++){
                    // push item
                    options[itarator].list.push($scope._internal.list[j]);
                    // get position of the list that we have to push
                    itarator = ((itarator === $scope._internal.columns - 1) ? 0 : (itarator+1)) ;
                }
                // return new options array
                $scope.listOptions = options;
            }],

            compile: function() {
                // return link function
                return function(scope, element){
                    // validate scrollable
                    if(scope._internal.resize) {
                        // todo implement custom resize function
                        // wait for the resize event
                        register.listen(scope.$on(api.tdResizable.resize, function(e){
                            // cancel event
                            register.cancelPropagation(e);
                            // apply changes
                            $timeout(function(){
                                // update widths
                                scope._internal = {
                                    width : (($(element.parent()).width() / $scope.options.columns) - (20 / 3)),
                                    height: $(element).parent().height()
                                };
                            }, 0);
                        }));
                    }
                    // validate scrollable
                    if(scope._internal.scroll) {
                        // get scrollable
                        var scrollable = $(element).parent();
                        // attach to jquery scroll event
                        scrollable.scroll(function () {
                            // get scroll
                            var scrollValue = scrollable.scrollTop() + scrollable.height();
                            // if we are in the 3/4 (or parameter) part of the scroll, emit event to load more data
                            if (!scope._internal.scrolling && (scrollValue > (scrollable[0].scrollHeight * (scope._internal.scroll.scrollLoadPosition || 0.9)))) {
                                // defer
                                var deferred = $q.defer();
                                // show loading
                                scope._internal.scrolling = true;
                                // mit event upwards so we can load more elements
                                scope.$broadcast(api.tdListEvents.manualScroll, true);
                                // last position of the array
                                var last = scope.options.list.length;
                                // mit event upwards so we can load more elements
                                scope.$emit(scope._internal.scroll.command ? scope._internal.scroll.command : api.tdMasonaryListEvents.scroll, last, deferred);
                                // digest elements
                                scope.$digest();
                                // iterator
                                var itarator = 0;
                                // wait until is loaded and finish loadin
                                deferred.promise.then(
                                    // on success
                                    function () {
                                        // iterate in list, and add one in each list
                                        for (var j = last; j < scope._internal.list.length; j++) {
                                            // push item
                                            scope.listOptions[itarator].list.push(scope._internal.list[j]);
                                            // get position of the list that we have to push
                                            itarator = ((itarator === scope._internal.columns - 1) ? 0 : (itarator + 1));
                                        }
                                    }
                                )['finally'](
                                    // on promise finished
                                    function () {
                                        // show loading
                                        scope._internal.scrolling = false;
                                        // todo refresh
                                        //set as loading false
                                        scope.$broadcast(api.tdListEvents.manualScroll, false);
                                    }
                                )
                            }
                        });
                    }
                }
            }
        };
    }]);
});