/**
 * Created by Guille on 23/12/2014.
 */
define(['../api'], function (api) {
    'use strict';

    // tdFormValidator
    api.directive('tdFormValidator', ['$log','$timeout', function ($log, $timeout) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',
            // link function
            link: function (scope, element, attr ) {
                // find closes ngForm
                var form = $(element).closest('.ng-form');
                // get form name
                var name = !form ? undefined : form.attr('name');
                // validate we have a form set
                if(!!name){
                    $timeout(function(){
                        // add form validation
                        element.attr(((!attr || (attr.formValidator && attr.formValidator == 'disable')) ? 'ng-disabled' : 'ng-show'),'!' + name + '.$valid');

                    },0);
                }
                else {
                    $log.error('tdFormValidator: need a ng-form with a name to bind to');
                }
            }
        };
    }]);
});