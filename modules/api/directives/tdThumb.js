/**
 * Created by guiyep on 14/09/15.
 */
define(['../api', 'angularFileUpload'], function (api) {
    'use strict';
    // directive extracted from angular-file-uploader
    api.directive('tdThumb', ['$window', 'tdModalService', function ($window, tdModalService) {
        // define helper
        var helper = {
            // this method says if the browser support canvas and file reader
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            // this means that is a file
            isFile: function (item) {
                // check instance of the item
                return angular.isObject(item) && item instanceof $window.File;
            },
            // this check if is an image
            isImage: function (file) {
                // ge file type
                var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                // return if it is a known extension
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };
        // return directive config
        return {
            // restrict to attribute
            restrict: 'A',

            template: '<canvas/>',

            link: function (scope, element, attrs) {
                // get attributes
                var params = scope.$eval(attrs.tdThumb);
                // check if we are supported
                if (helper.support && helper.isFile(params.file) && helper.isImage(params.file)) {
                    // get canvas element
                    var canvas = element.find('canvas');
                    // initialize file reader
                    var reader = new FileReader();
                    // this is triggered when file finished loading
                    reader.onload = function (event) {
                        // create new image
                        var img = new Image();
                        // set on load event
                        img.onload = function () {
                            // set width
                            var width = params.width || this.width / this.height * params.height;
                            // set height
                            var height = params.height || this.height / this.width * params.width;
                            // update canvar attrs
                            canvas.attr({width: width, height: height});
                            // draw canvas
                            canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                            // initialize modal dialog
                            var modalDialog = tdModalService({
                                model: {
                                    template: '<div>' +
                                            '<img src="' + event.target.result + '">' +
                                        '</div>',
                                    imageURL: event.target.result
                                }
                            });
                            // set click event
                            if(params.clickDisable !== true) {
                                // add click event
                                $(canvas).click(function () {
                                    // open and show image
                                    modalDialog.open();
                                });
                            }
                            // todo listen to destroy
                        };
                        // set src
                        img.src = event.target.result;
                    };
                    // read image as URL
                    reader.readAsDataURL(params.file);
                }
            }
        };
    }]);
});