/**
 * Created by guiyep on 5/05/15.
 */
define(['../api'], function (api) {
    'use strict';

    api.directive('tdToolbarContainer', [  function ( ) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            priority: 500,

            link: function(scope, element, attrs){
                // find td icons inside
                var icons = jQuery(element.find('.td-icon')).filter('[type]');
                // find expandable containers inside
                var expandables = jQuery(element.find('.td-item-expandable-container')).filter('[type]');
            }
        }
    }]);
});