/**
 * Created by Guille on 12/02/2015.
 */
define(['../api', 'jQuery', 'fullPage'], function (api, $) {
    'use strict';

    // tdFullPageEvents event Name
    api.tdFullPageEvents = {
        opened: 'tdFullPage-opened',
        closed: 'tdFullPage-closed',
        setSection: 'tdFullPage-set-section'
    };

    api.directive('tdFullPage', ['$compile','tdEventRegisterService','$document', '$window', 'tdPageLoaderService', '$log', 'tdRouterUrlChangerService', '$location', function ($compile, tdEventRegisterService, $document, $window, tdPageLoaderService, $log, tdRouterUrlChangerService, $location) {
        return {
            // we can only use this as class
            restrict: 'C',

            link: function(scope, element){
                // sections name arr
                var sectionsNames = [];
                // var url load
                var onUrlIdx = undefined;
                // get sections
                var sections = $.map(element.children(), function(el){
                    // get jq element
                    var jq = $(el);
                    // get id
                    var id = jq.attr('id');
                    // push id to sections names
                    sectionsNames.push('#'+ id);
                    // return section obj
                    return { section : jq , scope: undefined, id: id , icon: jq.attr('icon'), event: jq.attr('event') ? jq.attr('event') : undefined};
                });
                // initialize register service
                var register = tdEventRegisterService(element);
                // validate we have sections
                if(sectionsNames.length > 0 && sections.length == sectionsNames.length) {
                    // next element to show
                    var next = 0, from = 0;
                    // iterate in sections
                    $.each(sections, function(i, e){
                        // element
                        var jq = e.section;
                        // check which do we have to activate
                        if($location.path().indexOf(e.id)!= -1) {
                            // activate section depending on the url
                            e.section.addClass('active');
                            // save state
                            onUrlIdx = i;
                        }
                        // validate if we have any view or form to set
                        if((jq.attr('view') || jq.attr('form')) && jq.attr('id') && jq.attr('icon')){
                            // load template
                            tdPageLoaderService[jq.attr('view') ? 'view' : 'form'](jq.attr('view')).then(
                                // success function
                                function(data){
                                    // create template
                                    var el = angular.element(data);
                                    // add attr to element
                                    $(el).attr('ng-if', jq.attr('id'));
                                    // append element
                                    jq.append(el);
                                    // define new scope
                                    var s = scope.$new();
                                    // save scope
                                    sections[i].scope = s;
                                    // compile to parent scope new item
                                    $compile(el)(s);
                                }
                            );
                            // remove old attr
                            jq.removeAttr('view');
                            jq.removeAttr('form');
                            jq.removeAttr('controller');
                            jq.removeAttr('icon');
                        }
                    });
                    // define default options
                    scope.fullPageOptions = {
                        verticalCentered: false,
                        navigation: true,
                        css3: false,
                        normalScrollElements: sectionsNames.join(),
                        onLeave: function (fromIdx, toIdx) {
                            // execute inside apply
                            register.afterRender(function () {
                                // get section
                                var sectionObj = sections[toIdx - 1];
                                // set visibility
                                scope[sectionObj.id] = true;
                                // set next
                                next = toIdx - 1;
                                from = fromIdx - 1;
                                // digest changes
                                scope.$digest();
                                // broadcast event after load
                                sections[fromIdx - 1].scope.$broadcast(api.tdFullPageEvents.closed, sections[fromIdx - 1].id);
                            });
                        },
                        afterLoad: function () {
                            // digest changes
                            scope.$digest();
                            // broadcast event after load
                            sections[next].scope.$broadcast(api.tdFullPageEvents.opened, sections[next].id);
                            // set current url
                            tdRouterUrlChangerService($location.url().replace(sectionsNames[from].replace('#', '') , sectionsNames[next].replace('#', '') ));
                            // trigger that the section has changed
                            if(sections[next].event){
                                // emit event
                                scope.$emit(sections[next].event);
                            }
                        }
                    };
                    // apply changes
                    register.afterRender(
                        function () {
                            // iterate in a elements
                            $($document).find('#fp-nav li').each(function(i, e){
                                // find a
                                var li = $(e);
                                // empty
                                li.empty();
                                // get icon element
                                var icon = angular.element('<div class="td-icon" type="'+ sections[(onUrlIdx ? onUrlIdx : i)].icon +'" ng-click="moveTo('+(onUrlIdx ? onUrlIdx : i)+')"></div>');
                                // define move to
                                scope.moveTo = function(idx){
                                    // move to nex page
                                    $.fn.fullpage.moveTo(idx + 1);
                                };
                                // append
                                li.append(icon);
                                // compile
                                $compile(icon)(scope);
                            });
                            // get section
                            var sectionObj = sections[onUrlIdx ? onUrlIdx : 0];
                            // get scope
                            scope[sectionObj.id] = true;
                            // digest
                            scope.$digest();
                            // emit event
                            sectionObj.scope.$broadcast(api.tdFullPageEvents.opened, sectionObj.id);
                            // set url
                            var url = onUrlIdx ? $location.url() : $location.url() + '/' + sectionsNames[onUrlIdx ? onUrlIdx : 0].replace('#', '');
                            // set current url
                            tdRouterUrlChangerService(url);
                        }, false
                    );
                    // extend options if
                    angular.extend(scope.fullPageOptions, scope.options);
                    // listen budget
                    register.listen(scope.$on(api.tdFullPageEvents.setSection, function(event, id, f_Ok) {
                        // stop propagation of event
                        register.cancelPropagation(event);
                        // validate if we are in the current section
                        if(next !== id) {
                            // move to specific section
                            $.fn.fullpage.moveTo(id + 1);
                        }
                        // validate we have a defer
                        if(f_Ok){
                            // wait untill all is rendereds
                            register.afterRender(function(){
                                // resolve promise
                                f_Ok();
                            });
                        }
                    }));
                    // create full page
                    jQuery(element).fullpage(scope.fullPageOptions);
                    // on destroy
                    register.destroy(function () {
                        // remove window events
                        $($window)
                            .off('scroll')
                            .off('hashchange')
                            .off('resize');
                        // remove section events
                        $('.fp-section')
                            .off('click', '.fp-controlArrow');
                        // remove navigation
                        $('#fp-nav')
                            .off('click', '#fp-nav a')
                            .off('mouseenter', '#fp-nav li')
                            .off('mouseleave', '#fp-nav li')
                            .off('click', '.fp-slidesNav a')
                            .off('mouseover', scope.fullPageOptions.normalScrollElements)
                            .off('mouseout', scope.fullPageOptions.normalScrollElements)
                            .remove();
                        // iterate in sections
                        $.each(sections, function(i, e) {
                            // destroy scope
                            e.scope.$destroy();
                        });
                    });
                }
                else {
                    $log.error('need id defined in each section of the full page directive.')
                }
            }
        }
    }]);
});