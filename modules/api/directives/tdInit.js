/**
 * Created by guiyep on 14/08/15.
 */
define(['../api'], function (api) {
    'use strict';

    api.directive('tdInit', [ function () {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            priority: '500',

            scope: true,

            controller: ['$scope', '$attrs', function($scope, $attrs){
                // evaluate expression
                $scope.$eval($attrs.tdInit);
            }]
        }
    }]);
});