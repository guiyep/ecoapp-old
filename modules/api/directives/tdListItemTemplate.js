/**
 * Created by Guille on 15/01/2015.
 */
define(['../api'], function (api) {
    'use strict';
    api.directive('tdListItemTemplate', ['$compile', function ($compile) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            link: function (scope, element) {
                // extend properties
                angular.extend(scope, scope.item);
                // create template
                var aTemplate = angular.element(scope._internal.template);
                // append element
                element.append(aTemplate);
                // compile to parent scope new item
                $compile(aTemplate)(scope);
            }
        };
    }]);
});
