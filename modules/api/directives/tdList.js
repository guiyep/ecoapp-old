/**
 * Created by Guille on 30/12/2014.
 */
define(['../api', 'angularSanitize'], function (api) {
    'use strict';

    api.tdListEvents = {
        select : 'td-list-select',
        selected : 'td-list-selected',
        pageLoad: 'td-list-page-changed',
        scroll: 'td-list-scroll',
        manualScroll: 'td-list-manual-scroll'
    };

    api.directive('tdList', ['$log','$compile','tdEventRegisterService','$parse','tdPageLoaderService', '$q', '$timeout', function ($log, $compile, tdEventRegisterService, $parse, tdPageLoaderService, $q, $timeout) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            //TODO add scroll auto for list alone
            template: '<div class="td-list-container" ng-class="options.class" ng-style="calculate()">' +
                        '<pagination rotate="false" max-size="options.pagination.pagesShown" direction-links="false" ng-change="_events.pageChange()" items-per-page="options.pagination.pageSize" ng-if="options.pagination" boundary-links="true" total-items="_internal.total" ng-model="_internal.page"></pagination>' +
                        '<div class="td-list-item" ng-repeat="item in slice(options.list , _internal.start, limitsTo())">' +
                            '<div class="td-list-item-content" ng-if="_internal.template" ng-style="test()" ng-class="selected ? \'td-list-item-selected\' : \'\'" ng-click="_events.itemClick(item, $event)">' +
                                '<td-list-item-template class="td-list-item-content-template"></td-list-item-template>' +
                            '</div>' +
                        '</div>'+
                        '<div class="td-list-scroll-contianer" ng-if="_internal.scrolling">' +
                            '<span class="fa fa-spinner fa-pulse fa-4x"></span>' +
                        '</div>' +
                      '</div>',
            // link function
            controller: ['$scope','$element','$attrs', function($scope, $element, $attrs){
                // function that calculate width
                $scope.calculate = function(){
                    // get width
                    var width = $scope._internal.style.width;
                    // valdiate if we have ay width set
                    if(!width){
                        // expand to the max content of the container
                        $scope._internal.style.width = $($element.parent()).width();
                        // set width
                        width = $scope._internal.style.width;
                    }
                    // return width (10 is the margin)
                    return { width : (width - 10) + 'px'};
                };
                // define getter
                var getter = $parse($attrs.ngModel);
                // splice function
                $scope.slice = function(arr, start, end){
                    // get value
                    var modelValue = getter($scope);
                    // return new reference
                    return (arr || (modelValue ? modelValue : [])).slice(start, end) ;
                };
                // function to get start and end
                $scope.limitsTo = function(){
                    // return limit
                    return $scope.options.pagesShown ? $scope.options.pagesShown : $attrs.ngModel ? (getter($scope) || []).length : ($scope.options.list || []).length;
                };
                // define event element
                $scope._events = {};
                // we have the style object already initialized
                if($scope.options){
                    // initialize internal options
                    $scope._internal = {
                        start: 0,
                        end: $scope.limitsTo()
                    };
                    // check if we have height oor width set
                    if ($scope.options.height || $scope.options.width){
                        // set width
                        $scope._internal.style = $scope.options.width ? { width : $scope.options.width } : {};
                        // check if we have set the height
                        if($scope.options.height)
                            // set height
                            $scope._internal.style['height'] = $scope.options.height + 'px!important'
                    }
                    // check pagination
                    if($scope.options.pagination){
                        // show pages to be shown
                        $scope._internal.end = $scope.options.pagesShown || 10;
                        // set size by options
                        $scope._internal.total = $scope.options.totalSize || $attrs.ngModel ? (getter($scope) || []).length : ($scope.options.list || []).length || 0;
                        // set current page 1
                        $scope._internal.page = 1;
                    }
                    // check we have a src
                    if($scope.options.templateName){
                        // use ajax to load template
                        tdPageLoaderService.template($scope.options.templateName).then(
                            // on success
                            function( data ) {
                                // set template
                                $scope._internal.template = data;
                            }
                        );
                    }
                    // set template
                    if($scope.options.template && !$scope.options.templateName){
                        // set template
                        $scope._internal.template = $scope.options.template;
                    }
                }
            }],

            link: function(scope, element ){

                // initialize register service
                var register = tdEventRegisterService(element);
                // validate scrollable
                if(scope._internal.resize) {
                    // todo implement custom resize function
                    // wait for the resize event
                    register.listen(scope.$on(api.tdResizable.resize, function(e){
                        // cancel event
                        register.cancelPropagation(e);
                        // apply changes
                        $timeout(function(){
                            // update widths
                            scope.calculate();
                        }, 0);
                    }));
                }
                // define function
                var setPages = function() {
                    // save last selection
                    scope.lastSelected = undefined;
                    // minimum value
                    var minActualPage = (scope._internal.page - 1) * scope.options.pagination.pagesShown;
                    // max page
                    var maxActualPage = minActualPage + scope.options.pagination.pagesShown - 1;
                    // parameters that says if we have data to display
                    var hasData = maxActualPage < scope._internal.total;
                    // get difference
                    var dif = scope._internal.total - minActualPage ;
                    // validate no data but we have a remain to see
                    if(!hasData && (dif < scope.options.pagination.pagesShown)){
                        // update references
                        maxActualPage = (minActualPage + dif);
                        hasData = true;
                    }
                    // bind list function
                    var bindPages = function() {
                        // change page inside apply
                        register.afterRender(function () {
                            // set visible items
                            scope._internal.start = minActualPage;
                            scope._internal.end = maxActualPage + 1;
                            // digest elements
                            scope.$digest();
                        });
                    };
                    // check if we have data
                    if(hasData){
                        bindPages()
                    }
                    // validate we have defined change event
                    if(scope.options.pagination.commands && scope.options.pagination.commands.pageLoad) {
                        // defer
                        var deferred = $q.defer();
                        // mit event upwards so we can load more elements
                        scope.$emit(api.tdListEvents.pageLoad, scope._internal.page, minActualPage, maxActualPage + 1, hasData, deferred);
                        // wait until is loaded and finish loadin
                        deferred.promise.then(
                            // on success
                            function(){
                                // bind pages
                                bindPages();
                            },
                            // on error
                            function(error){
                                // log error
                                $log.error('error loading pages: ' + error);
                            }
                        )
                    }
                };
                // set overflow and screen withs
                var setOverflowSizes = function(){
                    // parse to jquery object
                    var jElementContainer = scope.options.scroll && scope.options.scroll.container ? scope.options.scroll.container : $(element).parent() ;
                    // get items
                    var items = $(element).find('.td-list-item');
                    // sum element in one variable
                    var sum = 0;
                    // iterate
                    $.each(items, function(i, e){
                        sum += $(e).children().height();
                    });
                    // validate items
                    if(sum > jElementContainer.height() ){
                        // set scroll style
                        jElementContainer.css('overflow-y', 'scroll' );
                    }
                };
                // select function
                var select = function(row){
                    // current scope
                    var targetScope = angular.element(row).scope();
                    // get scope reference (maybe was destroyed)
                    var lastScope = angular.element(scope.lastSelected).scope();
                    // check if we had something selected and set css
                    if(lastScope){ lastScope.selected = false }
                    // check if we are selected and perform action
                    if(!targetScope.selected) {
                        // save last selected
                        scope.lastSelected = row;
                        // emit selection
                        scope.$emit(api.tdListEvents.selected, targetScope.item);
                        // update css
                        targetScope.selected = true;
                    }
                };
                // check pagination
                if(scope.options.pagination) {
                    // define page change
                    scope._events.pageChange = setPages
                }
                // check pagination
                if(scope.options.scroll) {
                    // check pagination
                    if(scope.options.scroll.type == 'manual') {
                        // watch for manual scrolling
                        register.listen(scope.$on(api.tdListEvents.manualScroll, function(e, status){
                            // cancel event
                            register.cancelPropagation(e);
                            // show loading
                            scope._internal.scrolling = !!status;
                        }));
                    }
                    // get scrollable
                    var scrollable = $(element).parent();
                    // attach to jquery scroll event
                    scrollable.scroll(function(){
                        // get scroll
                        var scrollValue = scrollable.scrollTop() + scrollable.height();
                        // if we are in the 3/4 (or parameter) part of the scroll, emit event to load more data
                        if(!scope._internal.scrolling && (scrollValue > (scrollable[0].scrollHeight * (scope.options.scroll.scrollLoadPosition || 0.9)))){
                            // defer
                            var deferred = $q.defer();
                            // mit event upwards so we can load more elements
                            scope.$emit(scope.options.scroll.command ? scope.options.scroll.command : scope.tdListEvents.scroll, scope.limitsTo(), deferred);
                            // show loading
                            scope._internal.scrolling = true;
                            // digest elements
                            scope.$digest();
                            // wait until is loaded and finish loadin
                            deferred.promise['finally'](
                                // on promise finished
                                function(){
                                    // show loading
                                    scope._internal.scrolling = false;
                                }
                            )
                        }
                    });
                }

                // click event
                scope._events.itemClick = function(item, event){// current scope
                    // validate we are selectable
                    if(scope._internal.selectable)
                        // select item
                        select(angular.element(event.currentTarget));
                };
                // watch collection changes after bind was triggered
                register.listen(scope.$on(api.tdListEvents.select, function(e, expression){
                    // cancel event
                    register.cancelPropagation(e);
                    // validate we are selectable
                    // validate we are selectable
                    if(scope._internal.selectable) {
                        // iterate in visible items scope
                        $.each($(element).find('.td-list-item-content'), function (idx, item) {
                            // get object from element
                            var eItem = angular.element(item);
                            // evaluate expression (we can have only one selected)
                            if (eItem.scope().$eval(expression)) {
                                // trigger select function
                                select(eItem);
                            }
                        });
                    }
                }));
                // wait until all is rendered
                register.afterRender(function(){
                    // execute overflow functionality
                    setOverflowSizes();
                    // check if we have defined any pagination
                    if(!scope.options.pagination) {
                        // digest elements
                        scope.$digest();
                    }
                    else
                        // trigger internal function
                        setPages();
                });
            }
        };
    }]);
});