/**
 * Created by guiyep on 14/05/15.
 */
define(['../api'], function (api) {
    'use strict';

    api.tdToolbar = {
        setItem: 'td-toolbar-set-item',
        saveItem: 'td-toolbar-save-item',
        cancelItem: 'td-toolbar-save-cencel',
        changeStateToolbarItem: 'td-toolbar-change-toolbar-item'
    };

    api.directive('tdToolbar', ['tdEventRegisterService', '$timeout', '$q','$log', 'tdUniqueIdGenerator',
        'suitResourcesService',
        function (tdEventRegisterService, $timeout, $q, $log, tdUniqueIdGenerator,
        suitResourcesService) {
        return {
            // restrict to attribute or element or class name
            restrict: 'AEC',

            template: '<div class="toolbar">' +
                    '<div class="toolbar-buttons">' +
                            '<div class="toolbar-buttons-form">' +
                                '<div class="toolbar-buttons-form-button" ng-class="onAction ? \'toolbar-buttons-form-button-disabled\' : \'\' " ng-click="cancel()" ng-disabled="onAction" ng-show="data" td-i18n>{Cancel | CANCEL}</div>' +
                                '<div class="toolbar-buttons-form-button" ng-class="onAction ? \'toolbar-buttons-form-button-disabled\' : \'\' " ng-click="save()" ng-disabled="onAction" ng-show="data" ng-show="data" td-i18n>{Save | SAVE}</div>' +
                                // TODO implement custom button actions
                            '</div>' +
                            '<div class="toolbar-buttons-action">' +
                                '<div class="toolbar-buttons-action-button" ng-click="customAction(action)" ng-disabled="action.disabled" ng-class="action.disabled ? \'toolbar-buttons-form-button-disabled\' : \'\' " title="{{action.tooltip}}" ng-repeat="action in toolbarOptions.custom">' +
                                    '<i class="toolbar-buttons-action-button-button td-toolbar-item-custom" ng-init="options=action" ng-class="action.class" title="{{action.tooltip}}"></i>' +
                                '</div>' +
                                '<div class="toolbar-buttons-action-button" ng-click="performAction(action)" ng-disabled="action.disabled" ng-class="action.disabled ? \'toolbar-buttons-form-button-disabled\' : \'\' " title="{{action.tooltip}}" ng-repeat="action in toolbarOptions.tools">' +
                                    '<i class="toolbar-buttons-action-button-button" ng-class="action.class" ></i>' +
                                '</div>' +
                            '</div>' +
                    '</div>' +
                    '<div id="{{uniqueIdentifier}}" class="toolbar-form-container td-loading">' +
                        '<td-item-expandable-container class="toolbar-buttons-actions-forms" click="disabled" ng-show="toolbarItem.entity == action.entity" close-event="{{action.close}}" open-event="{{action.open}}" ng-repeat="action in toolbarOptions.tools">' +
                            '<div class="expanded">' +
                                '<td-form form-name="{{action.form}}" entity-name="{{action.entity}}" ng-model="data"></td-form>' +
                            '</div>' +
                        '</td-item-expandable-container>' +
                    '</div>'+
                '</div>',

            // configure directive
            controller: ['$scope', function($scope){
                // check tools
                if($scope.options.tools) {
                    // define internal tools
                    $scope.toolbarOptions = {
                        tools: [],
                        custom: []
                    };
                    // define unique identifier
                    $scope.uniqueIdentifier = tdUniqueIdGenerator();
                    // iterate in tools
                    for (var i = 0; i < $scope.options.tools.length; i++) {
                        // validate entity
                        if($scope.options.tools[i].entity) {
                            // initialize new item
                            $scope.toolbarOptions.tools.push($scope.options.tools[i]);
                            // set toolbar close and open
                            $scope.toolbarOptions.tools[i].uniqueIdentifier = tdUniqueIdGenerator();
                            // set toolbar close and open
                            $scope.toolbarOptions.tools[i].open = $scope.options.tools[i].open ? $scope.options.tools[i].open : 'open-' +$scope.options.tools[i].entity + '-' + $scope.toolbarOptions.tools[i].uniqueIdentifier;
                            // set toolbar close and open
                            $scope.toolbarOptions.tools[i].close = $scope.options.tools[i].close ? $scope.options.tools[i].close : 'close-' +$scope.options.tools[i].entity + '-' +$scope.toolbarOptions.tools[i].uniqueIdentifier;
                            // set toolbar close and open
                            $scope.toolbarOptions.tools[i].form = $scope.options.tools[i].form ? $scope.options.tools[i].form : $scope.options.tools[i].entity;
                            // set toolbar close and open
                            $scope.toolbarOptions.tools[i].name = $scope.options.tools[i].name ? $scope.options.tools[i].name : $scope.options.tools[i].entity;
                            // set disabled initial state, we reduce the amount of event sent
                            $scope.toolbarOptions.tools[i].disabled = $scope.options.tools[i].disabled;
                        }
                        else{
                            $log.error('the toolbar need at least the entity set');
                        }
                    }
                    // validate if we have any sustom action
                    if($scope.options.custom) {
                        // iterate in tools
                        for (var i = 0; i < $scope.options.custom.length; i++) {
                            // validate entity
                            if ($scope.options.custom[i].event || $scope.options.custom[i].directive) {
                                // initialize new item
                                $scope.toolbarOptions.custom.push($scope.options.custom[i]);
                                // set toolbar close and open
                                $scope.toolbarOptions.custom[i].uniqueIdentifier = tdUniqueIdGenerator();
                            }
                            else {
                                $log.error('the toolbar need at least the event or directive set');
                            }
                        }
                    }
                }
            }],

            link: function(scope, element){
                // define last opened
                var last;
                // set register
                var register = tdEventRegisterService(element);
                // listen start event
                register.listen(scope.$on(api.tdToolbar.setItem, function (event, type, op) {
                    // validate we are note disabled
                    if(!scope.onAction) {
                        // current action
                        var action;
                        // stop propagation of event
                        register.cancelPropagation(event);
                        // define if we already load the action
                        var loaded = false;
                        // set section
                        for (var i = 0; i < $scope.toolbarOptions.tools.length; i++) {
                            // get current action
                            action = scope.toolbarOptions.tools.actions[i];
                            // validate the type
                            if (action.name === type) {
                                // set current item first the element
                                scope.performAction(action, op ? op : undefined);
                                // set as loaded
                                loaded = true;
                                // break
                                break;
                            }
                        }
                        // if not loaded
                        if (!loaded) {
                            // set section
                            for (var i = 0; i < $scope.toolbarOptions.custom.length; i++) {
                                // get current action
                                var customAction = scope.toolbarOptions.custom[i];
                                // validate the type
                                if (customAction.name === type) {
                                    // validate custom action
                                    if (scope.customAction) {
                                        // set current item first the element
                                        scope.customAction(action, op ? op : undefined);
                                        // set as loaded
                                        loaded = true;
                                    }
                                    // break
                                    break;
                                }
                            }
                        }
                    }
                }));
                // define custom action
                scope.customAction = function (action, op){
                    // validate we are note disabled
                    if(!scope.onAction && !action.disabled) {
                        // validate if we have any directive set
                        if (!action.directive) {
                            // trigger action
                            scope.$emit(action.event, action, op);
                        }
                    }
                };
                // listen start event
                register.listen(scope.$on(api.tdToolbar.changeStateToolbarItem, function (event, css, disabled) {
                    // stop propagation of event
                    register.cancelPropagation(event);
                    // validate we are note disabled
                    if(css){
                        // find element
                        var selector = jQuery(element).find('.' + css);
                        // check we have only one
                        if(selector.length === 1){
                            // set as disabled
                            var action = angular.element(selector).scope().action;
                            // apply changes
                            $timeout(function () {
                                // set disabled state
                                action.disabled = disabled ? disabled : !action.disabled;
                            },0);
                        }
                    };
                }));
                // open functionality
                scope.performAction = function (action, data) {
                    // validate we are note disabled
                    if(!scope.onAction) {
                        // render first the element
                        scope.toolbarItem = action;
                        // wait until all was updated
                        $timeout(function () {
                            // validate if we have any data
                            if (!data)
                            // remove data as a start point
                                delete scope.data;
                            // if we have something
                            if (last) {
                                // close it
                                scope.$broadcast(last.close);
                                // validate
                                if (last.open != action.open) {
                                    // set data
                                    scope.data = data || {};
                                    // open new one
                                    scope.$broadcast(action.open);
                                    // update last
                                    last = action;
                                }
                                else {
                                    // start all over again
                                    last = undefined;
                                }
                            }
                            else {
                                // set data
                                scope.data = data || {};
                                // open new one
                                scope.$broadcast(action.open);
                                // update last
                                last = action;
                            }
                        }, 0);
                    }
                };
                // save functionality
                scope.save = function () {
                    // validate we are note disabled
                    if(!scope.onAction) {
                        // define defer
                        var deferred = $q.defer();
                        // set as on action
                        scope.onAction = true;
                        // start loading
                        scope.$broadcast(api.loadingEvents.start, scope.uniqueIdentifier, suitResourcesService.i18n.translate('WAIT_SAVING', 'Wait while we are saving...'));
                        // save
                        scope.$emit(api.tdToolbar.saveItem, scope.toolbarItem, deferred);
                        //wait until all is done
                        deferred.promise.then(
                            // success
                            function () {
                                // remove data
                                scope.data = {};
                                // remove visibility
                                last = undefined;
                                scope.toolbarItem = undefined;
                            },
                            // error
                            function (error) {

                            }
                        )['finally'](
                            // on finally function
                            function () {
                                // remove on action
                                scope.onAction = false;
                                //end stop loading
                                scope.$broadcast(api.loadingEvents.end, scope.toolbarItem.uniqueIdentifier);
                            }
                        )
                    }
                };
                // save functionality
                scope.cancel = function () {
                    // validate we are note disabled
                    if(!scope.onAction) {
                        // remove data
                        scope.data = {};
                        // remove visibility
                        last = undefined;
                        scope.toolbarItem = undefined;
                        // save
                        scope.$emit(api.tdToolbar.cancelItem, entity);
                    }
                };
            }
        }
    }]);
});