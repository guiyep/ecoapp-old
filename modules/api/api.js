/**
 * Created by Guille on 25/09/2014.
 */
define([
    'angular'
    //'angularAnimate'
], function (angular) {
    'use strict';

    // return angular.module('api', ['ngAnimate', 'suit']);
    return angular.module('api', ['suit']);
});