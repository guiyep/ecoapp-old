/**
 * Created by Guille on 11/03/2015.
 */
define(['../api'], function (api) {
    'use strict';
    // this is the device properties
    api.device = {};
    // config new resource
    api.factory('tdRouterUrlChangerService', ['$location', '$route', '$rootScope', function ( $location, $route, $rootScope) {
        // return service object
        return function(path){
            // get original path
            var original = $location.path;
            // get current route
            var lastRoute = $route.current;
            // watch for changes in hte location
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                // undo change
                $route.current = lastRoute;
                // unregister
                un();
            });
            var loc = original.apply($location, [path]);
            // digest change
            $rootScope.$digest();
            // execute function and return result
            return loc;
        }
    }]);
});