/**
 * Created by Guille on 20/02/2015.
 */
define(['../api', 'jQuery'], function (api, $) {
    'use strict';
    // this is the device properties
    api.cache = {};
    // config new resource
    api.factory('tdPageLoaderService', ['suitParametersService', 'suitResourcesService', '$q', '$compile', function (suitParametersService, suitResourcesService, $q, $compile) {
        // get url ref
        var urlRef = suitResourcesService.web.data('REF');
        // function to load forms
        var loadForm = function(html, name, scope, element, controller){
            // define iv
            var div;
            // get cache
            api.cache[name] = html;
            // parse html
            var jqHtml = angular.element(html);
            // validate we have a controller
            if(controller) {
                // create div with controller
                div = angular.element('<div ng-controller="'+controller+'"></div>');
                // append html
                div.append(jqHtml);
                // compile
                $compile(div)(scope);
                // append to element
                element.append(div);
            }
            else{
                // append html to element
                element.append(jqHtml);
                // compile
                $compile(jqHtml)(scope);
            }
        };
        // load html
        var loadHtml = function(url, name, scope, element, controller, async){
            // define differ
            var deferred = $q.defer();
            // validate we have already cached
            if(api.cache[name]){
                // validate load
                if(scope && element) {
                    // load
                    loadForm(api.cache[name], name, scope, element, controller);
                }
                // resolve promise
                deferred.resolve(api.cache[name]);
            }
            else {
                // use ajax to load template
                $.ajax({ dataType: "html", url: url, async: async ? async : false,
                    // on success
                    success: function (data) {
                        // validate load
                        if(scope && element)
                            // load
                            loadForm(data, name, scope, element, controller);
                        // resolve promise
                        deferred.resolve(data);
                    },
                    // on error reject
                    error: $q.reject
                });
            }
            // return promise
            return deferred.promise;
        };
        // return service object
        return {
            // load view
            view: function(name, scope, element, controller, async){
                // get url
                var url = urlRef &&  urlRef.URL && urlRef.URL.VIEW.START && urlRef.URL.VIEW.END ? urlRef.URL.VIEW.START + name + urlRef.URL.VIEW.END : '../../../EcoWebsite/EcoApp/Views/' + name + '.html';
                // use ajax to load template
                return loadHtml(url, name, scope, element, controller, async);
            },
            // load form
            form: function(name, scope, element, controller, async){
                // get url
                var url = urlRef &&  urlRef.URL && urlRef.URL.FORM.START && urlRef.URL.FORM.END ? urlRef.URL.FORM.START + name + urlRef.URL.FORM.END : '../../../EcoWebsite/EcoApp/Views/Forms/' + name + '.html';
                // use ajax to load template
                return loadHtml(url, name, scope, element, controller, async);
            },
            // template
            template: function(name, scope, element, controller, async){
                // get url
                var url = urlRef &&  urlRef.URL && urlRef.URL.TEMPLATE.START && urlRef.URL.TEMPLATE.END ? urlRef.URL.TEMPLATE.START + name + urlRef.URL.TEMPLATE.END : '../../../EcoWebsite/EcoApp/Views/Templates/' + name + '.html';
                // use ajax to load template
                return loadHtml(url, name, scope, element, controller, async);
            }
        }
    }]);
});