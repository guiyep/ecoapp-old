/**
 * Created by guiyep on 16/04/15.
 */
define(['../api'], function (api) {
    'use strict';
    // this is the device properties, this should be saved in the cache
    var col = [];
    // config new resource
    api.factory('tdNavigationService', ['$location', '$route', '$rootScope', function ( $location, $route, $rootScope) {
        // return service object
        return {
            // update navigation ref
            update: function(obj){
                // save obj if we have any
                if(obj){
                    // push item to the collection
                    col.push(obj);
                }
                // otherwise return last
                return col[col.length - 1];
            },
            // get
            getLast: function(){
                // otherwise return last
                return col[col.length - 1];
            },

            // get item last before
            getBefore: function(){
                // otherwise return last
                return col[col.length - 2];
            },
            // get all the history
            getAll : function(){
                // otherwise return last
                return col;
            }
        }
    }]);
});