/**
 * Created by guiyep on 26/08/15.
 */
define(['../api'], function (api) {
    'use strict';
    // config new resource
    api.factory('tdGoogleMapJqueryLoader', ['$document', '$rootScope', 'tdEventRegisterService','tdPageLoaderService', 'suitErrorParser','$q', 'tdUniqueIdGenerator', '$window', function ($document, $rootScope, tdEventRegisterService, tdPageLoaderService, suitErrorParser, $q, tdUniqueIdGenerator, $window) {
        // initialize register service without element
        var register = tdEventRegisterService();
        // return service object
        var serv = {

            callbackFunctionName: function() {
                return '$.googleMapInitialize';
            },

            trigger: function(name){
                // return jquery function so we can execute the function
                return $[name];
            },

            isLoaded: function(){
                // get scripts to validate we already add the google map api
                var scripts = $document[0].getElementsByTagName('script');
                // iterate
                for (var i = 0; i < scripts.length; i++) {
                    // validate is the api
                    if (scripts[i].id == 'googleApi') {
                        // set as initialized
                        return true;
                    }
                }
                return false;
            },

            load: function(options){
                // create initialization function
                $(function ($){
                        // this will destro all markers
                        $.destroyMarkers = function(opts){
                            // remove markers
                            $.each(opts.markersCollection, function (i, e) {
                                // remove map reference
                                e.setMap(null);
                            });
                            // empty array
                            opts.markersCollection = [];
                        };
                        // save initial state
                        $.googleMapInitialState = {};
                        // set initialization variables function(constructor)
                        $.googleMapPluginInitialize = function(opts){
                            // initialize variables
                            $.googleMapInitialState.containerId = opts.containerId;
                            $.googleMapInitialState.options = opts.options || {};
                            $.googleMapInitialState.markersCollection = [];
                            $.googleMapInitialState.listeners = [];
                            $.googleMapInitialState.deferred = $q.defer();
                            // return map representation
                            return {
                                containerId : opts.containerId,
                                options: $.googleMapInitialState.options,
                                markersCollection: $.googleMapInitialState.markersCollection,
                                listeners: $.googleMapInitialState.listeners,
                                rendered: $.googleMapInitialState.deferred.promise
                            }
                        };
                        // initialize
                        $.init = function(opts, gMap){
                            // wait until finished rendering
                            // defin options
                            var options = angular.copy(opts.options) || {};
                            // defer execution
                            var deferred = $q.defer();
                            // set position function
                            var setPosition = function(pos){
                                // resolve promise
                                deferred.resolve(pos);
                            };
                            // execute get current position
                            $window.navigator.geolocation.getCurrentPosition(setPosition);
                            // wait until we have the position
                            return $q.when($window.navigator.geolocation ? deferred.promise : {}).then(
                                // success
                                function(location){
                                    // TODO cache location
                                    // get lang long
                                    var lat = location && location.coords.latitude ? location.coords.latitude : undefined;
                                    var long = location && location.coords.longitude ? location.coords.longitude : undefined;
                                    // initialize function that set the center
                                    gMap.panTo(new google.maps.LatLng((options && options.initialLat ? options.initialLat : lat), (options && options.initialLat ? options.initialLong : long)));
                                }
                            );
                        };
                        // configure function
                        $.configure = function(opts, conf){
                            // differ execution
                            return register.afterRender(function() {
                                // TODO here we can add all the configurations that we want
                                // validate we have a zoom set and only one amrker
                                if(conf && conf.zoom && opts.markersCollection && opts.markersCollection.length === 1 || (!opts.markersCollection && conf && conf.zoom)){
                                    // set zoom to map
                                    $.zoomGoogleMap(conf.zoom, opts);
                                }
                            });
                        };
                        // fit the screen to the markers position
                        $.fitGoogleMap = function(opts){
                            // wait until finished rendering
                            opts.rendered.then(
                                // on finish rendering
                                function(gMap){
                                    // lat long collection
                                    var latlng = [];
                                    // iterate and create lat long collections
                                    $.each(opts.markersCollection, function(i,el){
                                        // push lat long
                                        latlng.push(el.position);
                                    });
                                    // create bounds array
                                    var latLngBounds = new google.maps.LatLngBounds();
                                    // iterate in lat longs
                                    $.each(latlng, function(i, latLong){
                                        // extend bounds
                                        latLngBounds.extend(latLong);
                                    });
                                    // set center to bounds
                                    gMap.setCenter(latLngBounds.getCenter());
                                    // fit
                                    gMap.fitBounds(latLngBounds);
                                }
                            );
                        };
                        // set zoom over map
                        $.zoomGoogleMap = function(zoom, opts){
                            // wait until finished rendering
                            opts.rendered.then(
                                // on finish rendering
                                function(gMap){
                                    // function to set map
                                    gMap.setZoom(zoom);
                                }
                            );

                        };
                        // google map initialization function
                        $.googleMapInitialize = function(opts){
                            // get container Id
                            var containerId = !opts ? $.googleMapInitialState.containerId : (opts.containerId ? opts.containerId : undefined);
                            // google map options
                            var gOptions = !opts ? $.googleMapInitialState.options : (opts.options ? opts.options : {});
                            // define marks collection
                            var mCollection = !opts ? $.googleMapInitialState.markersCollection : (opts.markersCollection ? opts.markersCollection : []);
                            // define listeners
                            var listeners = !opts ? $.googleMapInitialState.listeners : (opts.listeners ? opts.listeners : []);
                            // deferred
                            var deferred = !opts ? $.googleMapInitialState.deferred : (opts.rendered ? opts.rendered : $q.defer());
                            // define g map
                            var gMapOpt = {
                                containerId : containerId,
                                options: gOptions,
                                markersCollection: mCollection,
                                listeners: listeners,
                                rendered: deferred.promise
                            };
                            // after rendered
                            register.afterRender(function () {
                                // register
                                var registerMap = function(){
                                    // get options initial lat long -33.758631, 151.280619
                                    var op = {
                                        center: new google.maps.LatLng(7, 7),
                                        zoom: (gOptions && gOptions.zoom) ? gOptions.zoom : 3,
                                        mapTypeId: (gOptions && gOptions.mapType) ? gOptions.mapType : google.maps.MapTypeId.ROADMAP
                                    };
                                    // validate gmap
                                    var gMap= new google.maps.Map(currentMapContainer[0], op);
                                    // load markers if any
                                    $.loadMarkers(undefined, gMapOpt);
                                    // create initialization promises
                                    var initPromises = [];
                                    // set geolocalization
                                    if(angular.isUndefined(gOptions.autolocalize) || !!gOptions.autolocalize)
                                    // push new promise, we need to pass the gMap since it is not yet initialized
                                        initPromises.push($.init(gMapOpt, gMap));
                                    // configure map with options
                                    initPromises.push($.configure(gMapOpt, undefined, gMap));
                                    // wait untill all is executed
                                    $q.all(initPromises).then(
                                        // on success
                                        function(){
                                            // resolve rendered promise
                                            deferred.resolve(gMap);
                                        }
                                    );

                                };
                                // get container
                                var currentMapContainer = $('#' + containerId);
                                // if we are already loaded
                                if(currentMapContainer.is(':visible')){
                                    // register map
                                    registerMap();
                                }
                                else{
                                    // after all is loaded, execute initialization parameters
                                    var unregister = angular.element(currentMapContainer).scope().$watch(
                                        // wait until container is visible
                                        function(){
                                            // watch for changes in the visibility of the container, since need to be visible to be initialized
                                            return currentMapContainer.is(':visible');
                                        },
                                        // execute on visibility change
                                        function(visible){
                                            // if it is visible
                                            if(visible) {
                                                // register map
                                                registerMap();
                                                // remove watch since it is already loaded
                                                unregister();
                                            }
                                        }
                                    );
                                    // ad it in case is destroyed before being visible
                                    listeners.push(unregister);
                                }
                            });
                            // return map representation
                            return gMapOpt;
                        };
                        // add event to google map
                        $.addEvent = function(opts, eventName, f){
                            // wait until finished rendering
                            opts.rendered.then(
                                // on finish rendering
                                function(gMap){
                                    // add listener to google map
                                    gMap.addListener(eventName, f);
                                }
                            )
                        };
                        // this is to destroy plugin
                        $.destroyGoogleMapsListeners = function(opts){

                        };
                        // define map refs
                        $.mapRefs = {};
                        // function that load markers
                        $.loadMarkers = function(markers, opts, conf){
                            // google map options
                            var options = opts.options;
                            // define marks collection
                            var mCollection = opts.markersCollection;
                            // define listeners
                            var listeners = opts.listeners;
                            // wait until finished rendering
                            opts.rendered.then(
                                // on finish rendering
                                function(gMap){
                                    // add markers
                                    if(markers || (options && options.markers && options.preload)){
                                        // parse markers into array
                                        var arr = markers || options.markers;
                                        // get markers
                                        var googleMarkers = angular.isArray(arr) ? arr : [arr];
                                        // iterate
                                        for(var i = 0; i< googleMarkers.length; i++) {
                                            // get ref
                                            var ref = googleMarkers[i];
                                            // validate marker
                                            if(ref.lat && ref.long && ref.title) {
                                                // create marker
                                                mCollection.push(new google.maps.Marker({
                                                    position: new google.maps.LatLng(ref.lat, ref.long),
                                                    map: gMap,
                                                    icon: ref.type ? ref.icon : '',
                                                    title: ref.title
                                                }));
                                                // marker extra info
                                                var mapRef = {
                                                    template: googleMarkers[i].template,
                                                    marker : mCollection[mCollection.length - 1]
                                                };
                                                // initialize data
                                                mapRef.data = {};
                                                // extend options
                                                angular.extend(mapRef.data, googleMarkers[i].data );
                                                // validate if we need to set any info window
                                                if(googleMarkers[i].template) {
                                                    // add listener
                                                    listeners.push(google.maps.event.addListener(mCollection[mCollection.length - 1], 'click', function (event) {
                                                        // initialize empty info window
                                                        var infoWindow = new google.maps.InfoWindow();
                                                        // open marker
                                                        if ($.mapRefs[event.latLng.G + '-' + event.latLng.K]) {
                                                            // get map ref
                                                            var current = $.mapRefs[event.latLng.G + '-' + event.latLng.K];
                                                            // define scope
                                                            var scopeNew = $rootScope.$new(true);
                                                            // set data to scope
                                                            angular.extend(scopeNew, current.data);
                                                            // get content
                                                            var content = angular.element('<div></div>');
                                                            // wait until we get the view
                                                            tdPageLoaderService.template(current.template, scopeNew, content ).then(
                                                                // success
                                                                function(){
                                                                    // wait after all is executed and get html since google need a string
                                                                    register.afterRender(function(){
                                                                        // set content to info windows
                                                                        infoWindow.setContent(content.html());
                                                                        // open it
                                                                        infoWindow.open(gMap, current.marker);
                                                                    });
                                                                },
                                                                // error
                                                                function(error){
                                                                    $log.error(suitErrorParser(error))
                                                                }
                                                            );
                                                        }
                                                    }));
                                                }
                                                // add reference
                                                $.mapRefs[mCollection[mCollection.length - 1].position.G +'-'+ mCollection[mCollection.length - 1].position.K] = mapRef;
                                            }
                                        }
                                        // if we have markers fit them
                                        if(mCollection.length > 0)
                                        // fit to markers
                                            $.fitGoogleMap(opts);
                                        // wait finish render
                                        register.afterRender(function(){
                                            // validate we have a config set
                                            if(conf){
                                                // configure map with options
                                                $.configure(opts, conf)
                                            }
                                        });
                                    }
                                }
                            );

                        };
                    }
                );
                // create script and use callback
                var script = $document[0].createElement('script');
                script.id = 'googleApi';
                script.type = 'text/javascript';
                script.src = 'http://maps.googleapis.com/maps/api/js?key=' + (!!options && !!options.key ? options.key : 'AIzaSyA_uSB6pm3bdb-jEQ6pJwQzFYhbUxSdOQU') + '&sensor=false&callback=' + serv.callbackFunctionName();
                $document[0].body.appendChild(script);
            }
        };
        // return ser
        return serv;
    }]);
});