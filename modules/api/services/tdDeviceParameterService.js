/**
 * Created by Guille on 12/11/2014.
 */
define(['../api'], function (api) {
    'use strict';
    // this is the device properties
    api.device = {};
    // config new resource
    api.factory('tdDeviceParameterService', ['suitParametersService', function (suitParametersService) {
        // TODO get device properties and update api.device
        // return service object
        return function(name, property, defaultValue){
            // get parameters
            var parameters = suitParametersService.get(name, 'web');
            // return element parameters for device
            return parameters && parameters[property] ? parameters[property] : defaultValue
        }
    }]);
});