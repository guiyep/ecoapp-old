/**
 * Created by guiyep on 13/10/15.
 */
define(['../api'], function (api) {
    'use strict';
    // on close
    api.tdModalService = {
        finish: 'td-modal-service-finish'
    };
    // config new resource
    api.factory('tdModalService', ['$uibModal', function ($uibModal) {
        // return service object
        return function(options){
            // isntance ref
            var instance;
            // local options
            var localOptions = angular.copy(options);
            // instance to open the model
            var openFunction = $uibModal.open;
            // define options
            var op = {
                // set template url based on options
                template: '<div class="td-form" ng-model="model" form-name="' + options.formName +'" ' + (options.controller ? 'controller="'+ options.controller +'"' : '' )+ '></div>',
                // set a controller for the modal dialog
                controller: 'tdModalDialogController',
                // re define options result
                result: {
                    // return new model
                    model : angular.copy(options.model || {})
                }
            };
            // if we have template url delete template
            if(localOptions.templateUrl)
                delete op.template;
            // if we already have a template reset default (thi will be customized by the implementer)
            if(localOptions.template)
                delete op.template;
            // extend optiosn and local optiosn
            angular.extend(localOptions, op);
            // define open deferred
            var openDeferred = $q.defer();
            // define service object
            var serv = {
                // open function
                open: function(){
                    // open and save reference
                    instance = openFunction(localOptions);
                    // when the instance is closed resolve promise with the model
                    instance.result.then(
                        // success
                        function(model){
                            // resolve promise with the updated model
                            openDeferred.resolve(model);
                        },
                        // error
                        function(error){
                            // reject promise with error
                            openDeferred.reject(error)
                        }
                    );
                    // return promise
                    return openDeferred.promise;
                },
                // close function
                close: function() {
                    // force close
                    instance.close(undefined);
                },
                // destroy function
                destroy: function(){
                    // force close
                    instance.close(undefined);
                    // TODO remove modal dialog
                },
                // actions
                events: {

                }
            };
            // return object with reference to actions
            return serv;
        }
    }]);
});