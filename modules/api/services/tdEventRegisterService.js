/**
 * Created by Guille on 11/11/2014.
 */
define(['../api'], function (api) {
    'use strict';
    // collection listeners
    var listeners = [];
    // config new resource
    api.factory('tdEventRegisterService', ['$log','$timeout', '$q', function ($log, $timeout, $q) {
        // return service object
        return function(element) {
            // local events
            var localEvents = [], internalDestroy = undefined, notifiedListenersIdx = [];
            // define undefined service
            var service = {};
            // validate we have an element
            if (angular.isObject(element) && !!element) {
                // push on destroy event
                localEvents.push(element.scope().$on('$destroy', function () {
                    // unbind all function
                    var unbindAll = function (jElement) {
                        // check if we need something to off
                        if(jElement){
                            if( jElement.off)
                            // unbind events
                                jElement.off();
                            // get childs
                            var childs = jElement.children();
                            // iterate in children
                            for (var i = 0; i < childs.length; i++) {
                                unbindAll($(childs[i]));
                            }
                        }
                    };
                    // validate and execute destroy function
                    if (internalDestroy)
                        internalDestroy();
                    // get jquery element
                    var jElement = $(element);
                    // remove all jquery events
                    unbindAll(jElement);
                    // remove element
                    jElement.remove();
                    // remove element reference
                    jElement = undefined;
                    // iterate in listeners
                    for (var j = 0; j < localEvents.length; j++) {
                        // remove watch
                        localEvents[j]();
                    }
                    // remove events references
                    service.remove();
                    service = undefined;
                }));
                // initialize service object
                service = {
                    // listen to event
                    listen: function (event) {
                        // validate parameters
                        if (event) {
                            // push new event
                            localEvents.push(event);
                        }
                        else {
                            $log.error('eventRegisterService: listen function needs parameters');
                        }
                    },
                    // listen to destory
                    'destroy': function (onDestroy) {
                        internalDestroy = onDestroy;
                    },
                    // remove all references
                    remove: function () {
                    },
                    // event local propagation
                    notification: function (name, f) {
                        // TODO Check how it works when is destroyed the object (update idxs and remove f)
                        // validate if is a function
                        if(angular.isFunction(f)){
                            // initialize value
                            listeners[name] = listeners[name] || [];
                            // get idx
                            notifiedListenersIdx.push({ name: name, idx: listeners[name].length - 1});
                            // add f
                            listeners[name].push(f);
                        }
                    }
                };
            }
            // create common functionality (you do not need to pass 4element to initialize the service)
            var common = {
                // function that differ code
                afterRender: function (f, apply) {
                    // check cwe have something defined
                    if (f) {
                        // deffer execution after page is rendered to set the scroll
                        return $timeout(
                            // timeout function
                            function () {
                                // execute function after timeout is executed
                                f();
                            }, 0, angular.isUndefined(apply) || apply
                        );
                    }
                    // return rejected promise
                    return $q.reject();
                },
                // cancelPropagation (up and down)
                cancelPropagation : function (event) {
                    // validate event
                    if(event && (event.stopPropagatio || event.preventDefault)){
                        if(event.stopPropagation){
                            event.stopPropagation();
                        }
                        if(event.preventDefault){
                            event.preventDefault();
                        }
                    }
                },
                // event local propagation
                notify : function(name){
                    // get args
                    var args = jQuery.grep(arguments, function(val, idx){ return idx > 0 });
                    // trigger change function
                    $.each(listeners[name], function(idx, f){
                        // execute function
                        f.apply(this, args);
                    })
                }
            };
            // extend with common functionality
            angular.extend(service, common);
            // return service
            return service;
        }
    }]);
});