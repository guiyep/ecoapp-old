/**
 * Created by guiyep on 17/09/15.
 */
define(['../api'], function (api) {
    'use strict';
    // config new resource
    api.factory('tdParameterGetter', ['$filter', function ($filter) {
        // function to parse to camel case
        var camelCase = function(input) {
            return input.toLowerCase().replace(/-(.)/g, function(match, group1) {
                return group1.toUpperCase();
            });
        };
        // revert camel case
        var revertCamel = function(input){
            return input.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase()
        };
        // return service object
        var serv = {
            // function that get parameters from attrs
            getParameters : function(element, op){
                // define options
                var options = op || {};
                // collection fo valid attrs
                var obj = {};
                // get properties from attr that start with td
                $.each($(element)[0].attributes, function(i, e){
                    // validate that start with fwk attr items
                    if(e.name.indexOf(options && options.startWidth ? options.startWidth : 'td-') === 0 && $filter('filter')(options.exclude || [], { name : e.name}).length === 0){
                        // push attribute
                        obj[camelCase(e.name.substring((options.startWidth ? (options.startWidth.length + 1) : 3), e.name.length))] = e.value;
                    }
                });
                // return obj
                return obj;
            },

            setAttr : function(control, parameters, op){
                // define options
                var options = op || {};
                // iterate in props
                for(var prop in parameters){
                    // validate is not excluded
                    if($filter('filter')(options.exclude || [], { name : prop}).length === 0){
                        // set attr
                        $(control).attr(revertCamel(prop), parameters[prop]);
                    }
                }
            }

        };
        // return serv
        return serv;
    }]);
});