/**
 * Created by guiyep on 25/06/15.
 */
define(['../api'], function (api) {
    'use strict';
    // config new resource
    api.factory('tdUniqueIdGenerator', [function () {
        // return service function
        return function (variable) {

            var delim = variable ? '' : '-';

            function S4() {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            }

            return (variable ? '_' : '') +(S4() + S4() + delim + S4() + delim + S4() + delim + S4() + delim + S4() + S4() + S4());
        };
    }]);
});