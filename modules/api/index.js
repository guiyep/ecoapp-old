/**
 * Created by Guille on 25/09/2014.
 */

define([
    './controllers/tdModalDialogController',
    './controls/tdAutocomplete',
    './directives/tdCalendarPicker',
    './controls/tdDatePicker',
    './controls/tdFileUpload',
    './controls/tdGoogleMap',
    './directives/tdIcon',
    './directives/tdInit',
    './directives/tdItemExpandableContainer',
    './directives/tdList',
    './directives/tdListItemTemplate',
    './directives/tdLoading',
    './directives/tdMasonaryList',
    './controls/tdMultiSelect',
    './directives/tdSideMenu',
    './directives/tdThumb',
    './directives/tdI18n',
    './directives/tdForm',
    './directives/tdFormValidator',
    './directives/tdFullPage',
    './directives/tdRequireField',
    './directives/tdTypeValidator',
    './directives/tdToolbar',
    './directives/tdToolbarItemCustom',
    './services/tdDeviceParameterService',
    './services/tdEventRegisterService',
    './services/tdGoogleMapJqueryLoader',
    './services/tdModalService',
    './services/tdNavigationService',
    './services/tdPageLoaderService',
    './services/tdParameterGetter',
    './services/tdRouterUrlChangerService',
    './services/tdUniqueIdGenerator'
], function () {
    // create css for module and add
    var link = document.createElement('link');
    link.type = 'text/less';
    link.rel = 'stylesheet';
    link.href = require.toUrl('./modules/api/content/api.less');
    document.getElementsByTagName("head")[0].appendChild(link);
    // wait until less finished loading
    less.registerStylesheets().then(
        function () {
            // Now refresh() or modifyVars() to parse the changes.
            less.refresh();
        }
    );
    // validate console
    if(window.console && console.log)
        console.log('API module was loaded correctly');
});