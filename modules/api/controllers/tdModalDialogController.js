/**
 * Created by guiyep on 13/10/15.
 */
define(['../api'], function (api) {
    'use strict';

    api.controller('tdModalDialogController', ['$scope','$element', '$modalInstance', 'tdEventRegisterService', function($scope, $element, $modalInstance, tdEventRegisterService) {
        // initialize register service
        var register = tdEventRegisterService($element);
        // watch changes in the model length
        register.listen($scope.$on(api.tdModalService.finish, function(e, model){
            // prevent propagation
            register.cancelPropagation(e);
            // close modal with the response of hte model
            $modalInstance.close(model);
        }));
    }]);
});
