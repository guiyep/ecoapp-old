/**
 * Created by Guille on 26/11/2014.
 */
define([
    'angular'
], function (angular) {
    'use strict';

    var model = angular.module('model', []);

    model.server = 'http://localhost:8080';

    return model;
});
