/**
 * Created by guiyep on 5/08/15.
 */
define(['../model'], function (model) {
    'use strict';
    // config new resource
    model.config(['modelServiceProvider',function(modelServiceProvider) {
        // initialize user service
        modelServiceProvider.add('Profile', ['base', function(base){
            // here you initialize the variables depends on the screen you are using
            var serviceParams = {
                // new instance method
                'new': function () {
                    // here you initialize the user model
                    return {};
                },
                // validate method
                valid: function(obj){
                }
            };
            // initialize base service
            var baseUser = base('/api/Profile', serviceParams.valid);
            // extend functionality
            serviceParams['post'] = baseUser.post;
            serviceParams['put'] = baseUser.put;
            serviceParams['get'] = baseUser.get;
            serviceParams['delete'] = baseUser.delete;
            // return new service
            return serviceParams;
        }]);
    }]);
});