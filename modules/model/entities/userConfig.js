/**
 * Created by Guille on 26/11/2014.
 */
define(['../model'], function (model) {
    'use strict';
    // config new resource
    model.config(['modelServiceProvider',function(modelServiceProvider) {
        // initialize user service
        modelServiceProvider.add('User', ['base', function(base){
            // here you initialize the variables depends on the screen you are using
            var serviceParams = {
                // new instance method
                'new': function () {
                    // here you initialize the user model
                    return {};
                },
                // validate method
                valid: function(obj){
                    return obj.email && obj.password;
                },
                // here we define any custom functionality
                extra: {
                    login: function(obj){
                        return baseUser.get({email : obj.email, password: obj.password})
                    }
                }
            };
            // initialize base service
            var baseUser = base('/api/User', serviceParams.valid);
            // extend functionality
            serviceParams['post'] = baseUser.post;
            serviceParams['put'] = baseUser.put;
            serviceParams['get'] = baseUser.get;
            serviceParams['delete'] = baseUser.delete;
            // return new service
            return serviceParams;
        }]);
    }]);
});