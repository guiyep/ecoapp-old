/**
 * Created by Guille on 26/11/2014.
 */
define([
    './modelService',
    './entities/profileConfig',
    './entities/userConfig',
    './base'

], function () {
    // validate console
    if(window.console && console.log)
        console.log('Model module was loaded correctly');
});