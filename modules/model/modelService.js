/**
 * Created by Guille on 26/11/2014.
 */
define(['./model'], function (model) {
    'use strict';
    // config new resource
    model.provider('modelService',[function () {
        // different languages loaded
        var entities = {};
        // define base
        this.base = function(model) {
            // initialize directive
            entities['Base'] = model;
        };
        // define add functionality
        this.add = function(name,model) {
            // initialize directive
            entities[name] = model;
        };
        // function that get the service
        this.$get = [ function () {
            return {
                get: function(name){
                    // initialize injector
                    var $injector = angular.injector(['ng', 'suit']);
                    // here we have the base
                    var base = $injector.invoke(entities['Base']);
                    // return property
                    return entities[name] ? $injector.invoke(entities[name], this, {base: base}): undefined;
                }
            }
        }];
    }]);
});