/**
 * Created by Guille on 27/11/2014.
 */
define(['./model'], function (model) {
    'use strict';
    // config new resource
    // TODO - IMPLEMENT CACHE FACTORY, if we update we have to
    // TODO - update cache and this will trigger all the get functions that are attached to that entity
    model.config(['modelServiceProvider',function(modelServiceProvider) {
        // initialize user service
        modelServiceProvider.base(['$log','$http','$q','suitErrorParser', '$timeout', function($log, $http, $q, errorParser, $timeout){
            // return base logic
            return function(url, valid){
                // validate url
                if(angular.isString(url) && angular.isFunction(valid)) {
                    // define url
                    var URL = model.server + url;
                    // this method remove all unnecessary properties so the message is shorter and we remove noise
                    var serialize = function (obj) {
                       // iterate in obj TODO IMPLEMENT SERIALIZATION REMOVE UNNECESSARY PROP
                       return obj;
                    };
                    // return object with service
                    return {
                        // valid function
                        valid: valid,
                        // post method
                        post: function (obj) {
                            // prevent unnecessary post
                            if (this.valid(obj)) {
                                // post new user
                                return $http.post(URL, serialize(obj)).then(
                                    // success
                                    function (data) {
                                        // return data
                                        return data;
                                    },
                                    // error
                                    function (error) {
                                        // reject with error
                                        $q.reject(errorParser.parse(error));
                                    }
                                )
                            }
                            // return rejected promise
                            return $q.reject('You are trying to POST an invalid object');
                        },
                        // delete method
                        'delete': function (id) {
                            // prevent unnecessary post
                            if (angular.isNumber(id)) {
                                // post new user
                                return $http.delete(URL + '/:id', id).then(
                                    // success
                                    function (data) {
                                        // return data
                                        return data;
                                    },
                                    // error
                                    function (error) {
                                        // reject with error
                                        $q.reject(errorParser.parse(error));
                                    }
                                )
                            }
                            // return rejected promise
                            return $q.reject('You are trying to DELETE an invalid id');
                        },
                        // pit method
                        'put': function (obj) {
                            // prevent unnecessary post
                            if (valid(obj)) {
                                // post new user
                                return $http.put(URL, serialize(obj)).then(
                                    // success
                                    function (data) {
                                        // return data
                                        return data;
                                    },
                                    // error
                                    function (error) {
                                        // reject with error
                                        $q.reject(errorParser.parse(error));
                                    }
                                )
                            }
                            // return rejected promise
                            return $q.reject('You are trying to POST an invalid object');
                        },
                        // get method
                        get: function (params, method, query, differ) {
                            // prevent unnecessary get
                            if (this.valid(params) && differ) {
                                /*// parse params to ext to be added in the URL
                                var parseParams = function (obj) {
                                    // validate is object
                                    if (angular.isObject(obj)) {
                                        var str = '';
                                        for (var prop in obj) {
                                            str += obj[prop] + '/';
                                        }
                                        return str;
                                    }
                                    return obj;
                                };
                                // post new user
                                return $http.get(URL + '/' +( method ? method : '' )+ (params ? parseParams(params): '') + (query ? '?' + query : '') ).then(
                                    // success
                                    function (data) {
                                        // return data
                                        return data;
                                    },
                                    // error
                                    function (error) {
                                        // reject with error
                                        $q.reject(errorParser.parse(error));
                                    }
                                )*/
                                // TODO we need to implement the model
                                return $timeout(function(){
                                    return {};
                                } , 0)

                            }
                            // return rejected promise
                            return $q.reject('You are trying to GET with an invalid criteria');
                        }
                    }
                }
                $log.error('invalid URL for base service');
                return undefined;
            };
        }]);
    }]);
});