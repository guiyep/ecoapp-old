/**
 * Created by Guille on 11/11/2014.
 */
define(['../suit'], function (suit) {
    'use strict';
    // config new resource
    suit.provider('suitParametersService', [function () {
        // different languages loaded
        var directives = {};
        // define add functionality
        this.add = function(name,obj) {
            // initialize directive
            directives[name] = obj;
        };
        // function that get the service
        this.$get = [ function () {
            return {
                get: function(name, device){
                    // return property
                    return directives[name] ? directives[name][device] : undefined;
                }
            }
        }];
    }]);
});