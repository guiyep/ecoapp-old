/**
 * Created by Guille on 6/10/2014.
 */
define(['../suit'], function (suit) {
    'use strict';
    // config new resource
    suit.provider('suitResourcesService', [function () {
        // different languages loaded
        var languages = {};
        // different webs loaded
        var webs = {};
         // add language to languages reference
        this.addWebData = function (obj, identifier) {
            webs[identifier ? identifier : 'MAIN'] = obj;
        };
        // add language to languages reference
        this.addLanguage =  function(lang, obj) {
            languages[lang ? lang : 'DEFAULT'] = obj;
        };
        // function that get the service
        this.$get = ['$locale','$log', '$q', function ($locale, $log ) {
            return {
                i18n: {
                    translate: function (token, defaultText, lang) {
                        // get resource reference
                        var resource = languages[lang ? lang : 'DEFAULT'];
                       // return translation
                       return resource? ( resource[token] || defaultText ) :defaultText;
                    }
                },
                web: {
                    data: function (token, web) {
                        // get data
                        var data = webs[web ? web : 'MAIN'];
                        // validate we have a valid data
                        if(angular.isUndefined(data))
                            // log error
                            $log.error('invalid web data is being requested');
                        // return translation
                        return data[token] ? data[token] : undefined;
                    }
                }
            }
        }];
    }]);
});
