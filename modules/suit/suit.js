/**
 * Created by Guille on 25/09/2014.
 */
define([
    'angular'
], function (angular) {
    'use strict';

    return angular.module('suit', []);
});