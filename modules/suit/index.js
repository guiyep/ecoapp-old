/**
 * Created by Guille on 26/09/2014.
 */
define([
    './services/suitErrorParser',
    './services/suitParametersService',
    './services/suitResourcesService',
    './data/webDataConfig',
    './localization/default',
    './parameters/tdFormParameters',
    './parameters/tdSideMenuParameters'
], function () {
    console.log('Suit module was loaded correctly');
});