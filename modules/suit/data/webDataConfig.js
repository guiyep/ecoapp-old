/**
 * Created by Guille on 6/13/2014.
 */

define(['../suit', '../services/suitResourcesService'], function (suit) {
    'use strict';
    // config new resource
    suit.config(['suitResourcesServiceProvider', function (suitResourcesServiceProvider) {
        // get json file
       // var json = require("./Default/data.json");
        // add new language (DEFAULT)
        $.ajax({
            dataType: "json",
                url: "Modules/Suit/Data/Default/data.json",
            async: false,
            success: function( data ) {
                suitResourcesServiceProvider.addWebData(data);
            }
        });
    }]);
});

