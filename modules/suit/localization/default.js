/**
 * Created by Guille on 14/10/2014.
 */
define(['../suit', 'jQuery', '../services/suitResourcesService'], function (suit, $) {
    'use strict';
    // config new resource
    suit.config(['suitResourcesServiceProvider', function(suitResourcesServiceProvider) {
        $.ajax({
            dataType: "json",
            url: "Modules/Suit/Localization/en-US/en-US.json",
            async: false,
            success: function( data ) {
                // initialize en-US language
                suitResourcesServiceProvider.addLanguage(
                    'DEFAULT',
                    data
                );
            }
        });
    }]);
});