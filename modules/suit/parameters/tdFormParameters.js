/**
 * Created by Guille on 28/01/2015.
 */
/**
 * Created by Guille on 11/11/2014.
 */
define(['../suit', '../services/suitParametersService'], function (suit) {
    'use strict';
    // config new resource
    suit.config(['suitParametersServiceProvider', function(suitParametersServiceProvider) {
        // here you initialize the variables depends on the screen you are using
        suitParametersServiceProvider.add('tdForm', {
            default : {
                url: ''
            }
        });
    }]);
});