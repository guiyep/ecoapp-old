/**
 * Created by Guille on 25/09/2014.
 */
define([
    'angular'
], function (ng) {
    'use strict';

    return ng.module('views', ['model','suit','api']);
});