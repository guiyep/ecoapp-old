/**
 * Created by guiyep on 28/04/15.
 */
define(['./views'], function (views) {
    'use strict';

    views.event = {
        // general
        general: {
            modelUpdated : 'model-updated'
        },
        // calendar events
        request: {
            // on book click
            book: 'request-book',
            moreInfo: 'request-more-info'
        }

    }
});