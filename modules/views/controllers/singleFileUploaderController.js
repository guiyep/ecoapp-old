/**
 * Created by guiyep on 1/10/15.
 */
define(['../views'], function (views) {
    'use strict';

    views.controller('singleFileUploaderController', ['$scope', function($scope) {
        // define optiosn
        $scope.options = {
            ui: {
                showQueue: false,
                showUpdateAll: false,
                showCancelAll: false,
                showRemoveAll: false,
                showMultiple: false,
                showSingle: true,
                queueLength: 1,
                showThumb: true
            }
        }
    }]);
});