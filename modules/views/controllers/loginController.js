/**
 * Created by Guille on 31/10/2014.
 */
define(['../views', '../../api/api'], function (views, api) {
    'use strict';

    views.controller('loginController', ['$scope','modelService','$log','suitErrorParser',
        '$location','$rootScope', function($scope, modelService ,$log ,suitErrorParser ,$location ,$rootScope) {
            // get user model service
            var User = modelService.get('User');
            // initialize mode
            $scope.model = User.new();
            // submit click
            $scope.submit = function(){
                // start tdLoading
                $scope.$emit(api.loadingEvents.start, 'login-form');
                // post user
                User.extra.login($scope.model).then(
                    // success
                    function(response){
                        // validate we have one
                        if(response.data.length == 1) {
                            // we have a log user
                            $log.warn('User: ' + response.data[0].email);
                            // todo save user internally(cookies), we wil save it for now in the rootScope
                            $rootScope.user = response.data[0];
                            // change page
                            $scope.$apply(function(){
                                // redirect page
                                $location.path('/home');
                            });
                        }
                        else {
                            $log.error('We have duplicate data in the database, we need to purge data');
                        }
                    },
                    // error
                    function(err){
                        // log error
                        $log.error(suitErrorParser.parse(err));
                    }
                ).finally(function(){
                        // end tdLoading
                        $scope.$emit(api.loadingEvents.end);
                    }
                );
            }
        }]);
});