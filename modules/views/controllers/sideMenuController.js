/**
 * Created by Guille on 10/10/2014.
 */
define(['../views', '../../api/api'], function (views, api) {
    'use strict';

    views.controller('sideMenuController', ['$scope','$element','tdEventRegisterService','suitResourcesService', function($scope,$element, tdEventRegisterService, suitResourcesService) {
        // initialize register service
        var register = tdEventRegisterService($element);
        // get default values
        var list = suitResourcesService.web.data('MENU')['HOME'].list;
        // define menu options
        $scope.options =  {
            list : list,
            sourceId: 'logo',
            containerId: 'container',
            width: 200
        };
        // listen to menu changes
        register.listen($scope.$on(api.sideMenuEvents.menuOpened, function(event, item){
            // get resource
            var resource = suitResourcesService.web.data('MENU')[item.next];
            // check we have to update side menu items and update them
            if(item.next && resource){
                // initialize new list
                var newlist = suitResourcesService.web.data('MENU')[resource.parent] ? angular.copy(suitResourcesService.web.data('MENU')[resource.parent].list) || [] : [];
                $.merge(newlist, resource.list);
                $scope.options.list = newlist;
            }
        }));
    }]);
});
