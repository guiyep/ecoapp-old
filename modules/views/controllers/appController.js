/**
 * Created by Guille on 31/10/2014.
 */
define(['../views'], function (views) {
    'use strict';

    views.controller('appController', ['$scope','suitResourcesService', function($scope, suitResourcesService) {
        // define translation service
        $scope.i18n = {
            translate: suitResourcesService.i18n.translate
        };
        // define events reference
        $scope.events = views.event;
    }]);
});
