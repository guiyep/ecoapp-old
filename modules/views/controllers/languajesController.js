/**
 * Created by guiyep on 20/08/15.
 */
define(['../views'], function (views) {
    'use strict';

    views.controller('languajesController', ['$scope', '$filter', function($scope, $filter) {
        // get languajes
        var languajes = [
            { name: 'spanish', initials: 'ES' , id: 1},
            { name: 'english', initials: 'EN' , id: 2},
            { name: 'french', initials: 'fr' , id: 3}
        ];
        // languajes collection
        $scope.languajesSearch = function(value){
            // retunr filtr of the collection
            return $filter('filter')(languajes, { name : value });
        };
    }]);
});