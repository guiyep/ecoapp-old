/**
 * Created by guiyep on 14/08/15.
 */

define(['../../views', '../../../api/api'], function (views, api) {
    'use strict';

    views.controller('requestsController', ['$scope', '$element', 'tdEventRegisterService', '$timeout',function($scope, $element, tdEventRegisterService, $timeout) {
        $scope.options = {
            list: [
                {
                    defaultImg: {
                        smallSrc: '../Content/Images/bg1.jpg',
                        mediumSrc: '../Content/Images/bg1.jpg',
                        largeSrc: '../Content/Images/bg1.jpg'
                    },
                    map: {
                        positions: [
                            {
                                lat: -38.004995,
                                long: -57.551831,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'Sydney'}
                            },
                            {
                                lat: -38.012332,
                                long: -57.532519,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'NSW 2'}
                            }
                        ]
                    },
                    images: [
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg2.jpg',
                            mediumSrc: '../Content/Images/bg2.jpg',
                            largeSrc: '../Content/Images/bg2.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg1.jpg',
                            mediumSrc: '../Content/Images/bg1.jpg',
                            largeSrc: '../Content/Images/bg1.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        }
                    ],
                    createdBy: {
                        Id: 12345,
                        profileSrc: '../Content/Images/yo.jpg',
                        name: 'guillermo.polit'
                    },
                    title: 'Rome in depth',
                    startDate: new Date(),
                    endDate: new Date(),
                    type: {
                        name: 'FREE'
                    },
                    description: 'This is a trip inside Rome'
                },
                {
                    defaultImg: {
                        smallSrc: '../Content/Images/bg1.jpg',
                        mediumSrc: '../Content/Images/bg1.jpg',
                        largeSrc: '../Content/Images/bg1.jpg'
                    },
                    map: {
                        positions: [
                            {
                                lat: -39.004995,
                                long: -57.551831,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'Sydney 2'}
                            },
                            {
                                lat: -38.012332,
                                long: -57.532519,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'NSW 2'}
                            }
                        ]
                    },
                    images: [
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg2.jpg',
                            mediumSrc: '../Content/Images/bg2.jpg',
                            largeSrc: '../Content/Images/bg2.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg1.jpg',
                            mediumSrc: '../Content/Images/bg1.jpg',
                            largeSrc: '../Content/Images/bg1.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        }
                    ],
                    createdBy: {
                        Id: 12345,
                        profileSrc: '../Content/Images/yo.jpg',
                        name: 'guillermo.polit2'
                    },
                    title: 'Rome in depth 3',
                    anyDay: true,
                    comment: 'I do trips any day',
                    type: {
                        name: 'FREE'
                    },
                    description: 'This is a trip inside Rome 3'
                },
                {
                    defaultImg: {
                        smallSrc: '../Content/Images/bg1.jpg',
                        mediumSrc: '../Content/Images/bg1.jpg',
                        largeSrc: '../Content/Images/bg1.jpg'
                    },
                    map: {
                        positions: [
                            {
                                lat: -39.004995,
                                long: -57.551831,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'Sydney 2'}
                            },
                            {
                                lat: -38.012332,
                                long: -57.532519,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'NSW 2'}
                            }
                        ]
                    },
                    images: [
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg2.jpg',
                            mediumSrc: '../Content/Images/bg2.jpg',
                            largeSrc: '../Content/Images/bg2.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg1.jpg',
                            mediumSrc: '../Content/Images/bg1.jpg',
                            largeSrc: '../Content/Images/bg1.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        }
                    ],
                    createdBy: {
                        Id: 12345,
                        profileSrc: '../Content/Images/yo.jpg',
                        name: 'guillermo.polit2'
                    },
                    title: 'Rome in depth 3',
                    weekends: true,
                    comment: 'I do trips on any weekend',
                    type: {
                        name: 'FREE'
                    },
                    description: 'This is a trip inside Rome 3'
                },
                {
                    defaultImg: {
                        smallSrc: '../Content/Images/bg1.jpg',
                        mediumSrc: '../Content/Images/bg1.jpg',
                        largeSrc: '../Content/Images/bg1.jpg'
                    },
                    map: {
                        positions: [
                            {
                                lat: -39.004995,
                                long: -57.551831,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'Sydney 2'}
                            },
                            {
                                lat: -38.012332,
                                long: -57.532519,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'NSW 2'}
                            }
                        ]
                    },
                    images: [
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg2.jpg',
                            mediumSrc: '../Content/Images/bg2.jpg',
                            largeSrc: '../Content/Images/bg2.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg1.jpg',
                            mediumSrc: '../Content/Images/bg1.jpg',
                            largeSrc: '../Content/Images/bg1.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        }
                    ],
                    createdBy: {
                        Id: 12345,
                        profileSrc: '../Content/Images/yo.jpg',
                        name: 'guillermo.polit2'
                    },
                    title: 'Rome in depth 3',
                    weekdays: true,
                    comment: 'I do trips on any weekday',
                    type: {
                        name: 'FREE'
                    },
                    description: 'This is a trip inside Rome 3'
                },
                {
                    defaultImg: {
                        smallSrc: '../Content/Images/bg1.jpg',
                        mediumSrc: '../Content/Images/bg1.jpg',
                        largeSrc: '../Content/Images/bg1.jpg'
                    },
                    map: {
                        positions: [
                            {
                                lat: -39.004995,
                                long: -57.551831,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'Sydney 2'}
                            },
                            {
                                lat: -38.012332,
                                long: -57.532519,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'NSW 2'}
                            }
                        ]
                    },
                    images: [
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg2.jpg',
                            mediumSrc: '../Content/Images/bg2.jpg',
                            largeSrc: '../Content/Images/bg2.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg1.jpg',
                            mediumSrc: '../Content/Images/bg1.jpg',
                            largeSrc: '../Content/Images/bg1.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        }
                    ],
                    createdBy: {
                        Id: 12345,
                        profileSrc: '../Content/Images/yo.jpg',
                        name: 'guillermo.polit2'
                    },
                    title: 'Rome in depth 3',
                    weekends: true,
                    comment: 'I do trips on any weekend',
                    type: {
                        name: 'FREE'
                    },
                    description: 'This is a trip inside Rome 3'
                },
                {
                    defaultImg: {
                        smallSrc: '../Content/Images/bg1.jpg',
                        mediumSrc: '../Content/Images/bg1.jpg',
                        largeSrc: '../Content/Images/bg1.jpg'
                    },
                    map: {
                        positions: [
                            {
                                lat: -39.004995,
                                long: -57.551831,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'Sydney 2'}
                            },
                            {
                                lat: -38.012332,
                                long: -57.532519,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'NSW 2'}
                            }
                        ]
                    },
                    images: [
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg2.jpg',
                            mediumSrc: '../Content/Images/bg2.jpg',
                            largeSrc: '../Content/Images/bg2.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg1.jpg',
                            mediumSrc: '../Content/Images/bg1.jpg',
                            largeSrc: '../Content/Images/bg1.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        }
                    ],
                    createdBy: {
                        Id: 12345,
                        profileSrc: '../Content/Images/yo.jpg',
                        name: 'guillermo.polit2'
                    },
                    title: 'Rome in depth 3',
                    weekends: true,
                    comment: 'I do trips on any weekend',
                    type: {
                        name: 'FREE'
                    },
                    description: 'This is a trip inside Rome 3'
                },
                {
                    defaultImg: {
                        smallSrc: '../Content/Images/bg1.jpg',
                        mediumSrc: '../Content/Images/bg1.jpg',
                        largeSrc: '../Content/Images/bg1.jpg'
                    },
                    map: {
                        positions: [
                            {
                                lat: -39.004995,
                                long: -57.551831,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'Sydney 2'}
                            },
                            {
                                lat: -38.012332,
                                long: -57.532519,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'NSW 2'}
                            }
                        ]
                    },
                    images: [
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg2.jpg',
                            mediumSrc: '../Content/Images/bg2.jpg',
                            largeSrc: '../Content/Images/bg2.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg1.jpg',
                            mediumSrc: '../Content/Images/bg1.jpg',
                            largeSrc: '../Content/Images/bg1.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        }
                    ],
                    createdBy: {
                        Id: 12345,
                        profileSrc: '../Content/Images/yo.jpg',
                        name: 'guillermo.polit2'
                    },
                    title: 'Rome in depth 3',
                    weekends: true,
                    comment: 'I do trips on any weekend',
                    type: {
                        name: 'FREE'
                    },
                    description: 'This is a trip inside Rome 3'
                },
                {
                    defaultImg: {
                        smallSrc: '../Content/Images/bg1.jpg',
                        mediumSrc: '../Content/Images/bg1.jpg',
                        largeSrc: '../Content/Images/bg1.jpg'
                    },
                    map: {
                        positions: [
                            {
                                lat: -39.004995,
                                long: -57.551831,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'Sydney 2'}
                            },
                            {
                                lat: -38.012332,
                                long: -57.532519,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'NSW 2'}
                            }
                        ]
                    },
                    images: [
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg2.jpg',
                            mediumSrc: '../Content/Images/bg2.jpg',
                            largeSrc: '../Content/Images/bg2.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg1.jpg',
                            mediumSrc: '../Content/Images/bg1.jpg',
                            largeSrc: '../Content/Images/bg1.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        }
                    ],
                    createdBy: {
                        Id: 12345,
                        profileSrc: '../Content/Images/yo.jpg',
                        name: 'guillermo.polit2'
                    },
                    title: 'Rome in depth 3',
                    weekends: true,
                    comment: 'I do trips on any weekend',
                    type: {
                        name: 'FREE'
                    },
                    description: 'This is a trip inside Rome 3'
                },
                {
                    defaultImg: {
                        smallSrc: '../Content/Images/bg1.jpg',
                        mediumSrc: '../Content/Images/bg1.jpg',
                        largeSrc: '../Content/Images/bg1.jpg'
                    },
                    map: {
                        positions: [
                            {
                                lat: -39.004995,
                                long: -57.551831,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'Sydney 2'}
                            },
                            {
                                lat: -38.012332,
                                long: -57.532519,
                                type: '',
                                title: 'Test Marker',
                                template: 'googleInfoWindow',
                                data: {title: 'NSW 2'}
                            }
                        ]
                    },
                    images: [
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg2.jpg',
                            mediumSrc: '../Content/Images/bg2.jpg',
                            largeSrc: '../Content/Images/bg2.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/bg1.jpg',
                            mediumSrc: '../Content/Images/bg1.jpg',
                            largeSrc: '../Content/Images/bg1.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        },
                        {
                            smallSrc: '../Content/Images/indo.jpg',
                            mediumSrc: '../Content/Images/indo.jpg',
                            largeSrc: '../Content/Images/indo.jpg'
                        }
                    ],
                    createdBy: {
                        Id: 12345,
                        profileSrc: '../Content/Images/yo.jpg',
                        name: 'guillermo.polit2'
                    },
                    title: 'Rome in depth 3',
                    weekends: true,
                    comment: 'I do trips on any weekend',
                    type: {
                        name: 'FREE'
                    },
                    description: 'This is a trip inside Rome 3'
                }
            ],
            templateName: 'requestListItemTemplate',
            columns: 4,
            scroll: true
        };
        // initialize register service
        var register = tdEventRegisterService($element);
        // listen to scroll event, so we load more items
        // on reset markers
        register.listen($scope.$on(api.tdMasonaryListEvents.scroll, function(ev, top, resolution){
            // stop propagation
            register.cancelPropagation(ev);
            // wait until google maps is rendered
            $timeout(function(){
                $.merge($scope.options.list, angular.copy($scope.options.list));
                resolution.resolve();
            }, 1000);
        }));
    }]);
});