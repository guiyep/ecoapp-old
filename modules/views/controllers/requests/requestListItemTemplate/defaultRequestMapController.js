/**
 * Created by guiyep on 3/08/15.
 */
define(['../../../views'], function (views) {
    'use strict';

    views.controller('defaultRequestMapController', ['$scope', function($scope) {
        $scope.options = {
            preload: true,
            markers: $scope.map.positions,
            width: 'parent',
            height: '500',
            autolocalize: false
        }
    }]);
});