/**
 * Created by Guille on 31/10/2014.
 */
define(['../views', '../../api/api'], function (views, api) {
    'use strict';

    views.controller('registerController', ['$scope','modelService','$log','suitErrorParser',
        '$location','$rootScope', function($scope, modelService ,$log ,suitErrorParser ,$location ,$rootScope) {
            // get user model service
            var User = modelService.get('User');
            // initialize mode
            $scope.model = User.new();
            // submit click
            $scope.submit = function(){
                // start tdLoading
                $scope.$emit(api.loadingEvents.start, 'register-form');
                // post user
                User.post($scope.model).then(
                    // success
                    function(response){
                        // we have a log user
                        $log.warn('User: ' + response.data.email);
                        // todo save user internally(cookies), we wil save it for now in the rootScope
                        $rootScope.user = response.data;
                        // change page
                        $scope.$apply(function(){
                            // redirect page
                            $location.path('/home');
                        });
                    },
                    // error
                    function(err){
                        // log error
                        $log.error(suitErrorParser(err));
                    }
                ).finally(function(){
                        // end tdLoading
                        $scope.$emit(api.loadingEvents.end);
                    }
                );
            }
        }]);
});