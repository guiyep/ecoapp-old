/**
 * Created by guiyep on 20/08/15.
 */
define(['../../views'], function (views) {
    'use strict';

    views.controller('requestMapController', ['$scope', function($scope) {
        // option for map
        $scope.options = {
            preload: true,
            markers: [],
            width: 'parent',
            height: '500',
            loading: true,
            menu: [
                {
                    name: 'set center here', command: 'set-center'
                },
                {
                    name: 'add to route', command: 'add-route'
                }
            ]
        };

    }]);
});