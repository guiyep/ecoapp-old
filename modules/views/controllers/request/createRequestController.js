/**
 * Created by guiyep on 19/08/15.
 */
define(['../../views', '../../../api/api'], function (views, api) {
    'use strict';

    views.controller('createRequestController', ['$scope', '$element', 'tdEventRegisterService', '$timeout',function($scope, $element, tdEventRegisterService, $timeout) {
        // set place to display
        var markers = [];
        // define markes collection
        var markersAndRoute = [];
        // initialize model
        $scope.model ={
            // crated by
            profile: {
                Id: 12345,
                profileSrc: '../content/images/yo.jpg',
                name: 'guillermo.polit'
            },
            //when: new Date(),
            _today: new Date()
        };
        // initialize register service
        var register = tdEventRegisterService($element);
        // watch changes in the model length
        register.listen($scope.$on('routes-updated', function(e, collection){
            // stop event propagation
            register.cancelPropagation(e);
            // define markes collection
            markersAndRoute = [];
            // set current place
            markersAndRoute[0] = markers[0];
            // iterate
            $.each(collection, function(i,e){
                // define new item // TODO create place reference same than the google geocode result
                var newItem = {
                        lat: angular.isFunction(e.geometry.location.lat) ? e.geometry.location.lat() : e.geometry.location.lat,
                        long: angular.isFunction(e.geometry.location.lng) ? e.geometry.location.lng() : e.geometry.location.lng,
                        type: '',
                        title: e.formatted_address,
                        template: 'googleInfoWindow',
                        data: {title: e.formatted_address}
                    };
                // push item
                markersAndRoute.push(newItem);
            });
            // validate length
            if(collection.length !== 0)
                // broadcast set markers event
                $scope.$broadcast(api.tdGoogleMap.setMarkers, markersAndRoute);
            else
                // broadcast set markers event
                $scope.$broadcast(api.tdGoogleMap.setMarkers, markers, { zoom : 8 });
        }));
        // watch changes in the model length
        register.listen($scope.$on('add-route', function(e, addr){
            // stop event propagation
            register.cancelPropagation(e);
            // broadcast add to the multiselect
            $scope.$broadcast('add-multi-routes', addr[0]);
        }));
        // watch changes in the model length
        register.listen($scope.$on('set-center', function(e, addr){
            // stop event propagation
            register.cancelPropagation(e);
            // set place
            $scope.model.place = angular.copy(addr[1]);
        }));
        // lsiten to changes in hte place
        register.listen($scope.$watch('model.place', function(value, old){
            // validate if we have geometry
            if(angular.isObject(value)){
                // broadcast add to the multiselect
                $scope.$broadcast('clear-routes-multiselect');
                // define lat
                var lat = angular.isFunction(value.geometry.location.lat) ? value.geometry.location.lat() : value.geometry.location.lat;
                var lng = angular.isFunction(value.geometry.location.lng) ? value.geometry.location.lng() : value.geometry.location.lng;
                // validate we are a new location
                if(angular.isUndefined(markers[0]) || markers[0] && markers[0].lat !== lat && markers[0].long !== lng) {
                    // new marker
                    markers[0] = {
                        lat: lat ,
                        long: lng,
                        type: '',
                        title: value.formatted_address,
                        template: 'googleInfoWindow',
                        data: {title: value.formatted_address}
                    };
                    // broadcast set markers event
                    $scope.$broadcast(api.tdGoogleMap.setMarkers, markers, { zoom : 8 });
                }
            }
            // validate we are not resetting
            if(angular.isObject(old) && !angular.isObject(value)){
                // reset marker
                markers[0] = undefined;
                // broadcast set markers event
                $scope.$broadcast(api.tdGoogleMap.resetMarkers);
                // broadcast add to the multiselect
                $scope.$broadcast('clear-routes-multiselect');
            }
        }, true));
    }]);
});