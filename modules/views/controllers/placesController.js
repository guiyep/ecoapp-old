/**
 * Created by Guille on 19/11/2014.
 */
// https://maps.googleapis.com/maps/api/place/textsearch/xml?query=Mar+Del+Plata&sensor=true&key=AIzaSyBLjofDPFGqsCM4UL7WEc7lN5vnFy7xRDM
// Any function returning a promise object can be used to load values asynchronously
define(['../views', '../../api/api'], function (views, api) {
    'use strict';

    views.controller('placesController', ['$scope','$http','$log', function($scope, $http, $log) {
        $scope.getLocation = function(val) {

            return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    address: val,
                    sensor: false
                }
            }).then(
                // success
                function(response){
                    // parse response
                    return response.data.results.map(function(item){
                            // return address
                            return item;
                        },
                        // error
                        function(){
                            $log.error('Error tdLoading google places')
                        }
                    );
                },
                // error
                function(){
                    $log.error('Error tdLoading google places')
                }
            );
        };
    }]);
});

