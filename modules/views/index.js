/**
 * Created by Guille on 26/09/2014.
 */
define([
    './events',
    './controllers/profile/profileController',
    './controllers/request/createRequestController',
    './controllers/request/requestMapController',
    './controllers/requests/requestListItemTemplate/defaultRequestMapController',
    './controllers/requests/requestListItemTemplate/requestListItemTemplateController',
    './controllers/requests/requestsController',
    './controllers/appController',
    './controllers/frontController',
    './controllers/homeController',
    './controllers/languajesController',
    './controllers/loginController',
    './controllers/placesController',
    './controllers/registerController',
    './controllers/sideMenuController',
    './controllers/singleFileUploaderController',
    // directives
    './directives/vwClickable',
    './directives/vwEvent',
    './directives/vwUserProfile'
], function () {
    // create css for module and add
    var link = document.createElement('link');
    link.type = 'text/less';
    link.rel = 'stylesheet';
    link.href = require.toUrl('./modules/Views/Content/views.less');
    document.getElementsByTagName("head")[0].appendChild(link);
    // wait until less finished loading
    less.registerStylesheets().then(
        function () {
            // Now refresh() or modifyVars() to parse the changes.
            less.refresh();
        }
    );
    // validate console
    if(window.console && console.log)
        console.log('Views module was loaded correctly');
});
