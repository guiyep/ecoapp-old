
/**
 * Created by guiyep on 5/05/15.
 */
define(['../views'], function (views) {
    'use strict';

    views.directive('vwClickable', [ '$parse', 'tdEventRegisterService', function ( $parse, tdEventRegisterService) {
        return {
            // restrict to attribute or element or class name
            restrict: 'A',

            link: function(scope, element, attr){
                // validate if we have to show mose over effect
                if(angular.isUndefined(attr.vwClickableEffects) || attr.vwClickableEffects === true)
                    // add css class
                    element.addClass('vw-clickable-effects');
                // initialize register service
                var register = tdEventRegisterService(element);
                // get op
                jQuery(element).click(function(e){
                    // define getter
                    var getter = $parse(attr.vwClickable);
                    // value
                    var event = getter(views.event);
                    // emit event upwards
                    scope.$emit(event, e);
                });
                // on destroy
                register.destroy(function(){
                    // unbind click
                    jQuery(element).unbind('click');
                })
            }
        }
    }]);
});