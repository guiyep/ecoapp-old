/**
 * Created by guiyep on 5/08/15.
 */
define(['../views'], function (views) {
    'use strict';

    views.directive('vwUserProfile', ['$log', 'modelService', function ($log, modelService) {
        return {
            // restrict to attribute or element or class name
            restrict: 'C',

            scope: {
                // set modal same as parent
                profile: '=ngModel'
            },

            require: 'ngModel',

            template: '<img class="vw-user-profile-img" ng-src="{{profile.profileSrc}}" ng-click="openProfile(profile.Id)">' +
            '<div class="vw-user-profile-name" >' +
                '<span ng-click="openProfile(profile.Id)">#{{ profile.name | uppercase }}</span>' +
            '</div>',

            link: function(scope, element, attr, ngModel){
                // define open function
                scope.openProfile = function (Id) {
                    alert('implement opening of profileId ' + Id);
                };
                // validate we have the Id set of the profile
                if(scope.profile.Id) {
                    // get profile of user
                    var Profile = modelService.get('Profile');
                    // get user profile, we do not need to cache since the model is in charge of cache objects and update them
                    Profile.get({Id: scope.profile.Id}, function (single) {
                        // apply changes
                        scope.$apply(function () {
                            // extend properties
                            angular.extend(ngModel.$viewValue, single);
                        });
                    }, undefined, undefined, !(scope.profile.profileSrc && scope.profile.name));
                }
            }
        }
    }]);
});