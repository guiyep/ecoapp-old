/**
 * Created by guiyep on 5/05/15.
 */
define(['../views'], function (views) {
    'use strict';

    views.directive('vwEvent', [ '$parse', '$log', function ( $parse, $log) {
        return {
            // restrict to attribute or element or class name
            restrict: 'A',

            priority: '500',

            link: function(scope, element, attr){
                // get op
                var options = scope.$eval(attr.vwEvent);
                // update ref
                options = !angular.isObject(options) ? attr.vwEvent : options;
                // define is valid
                if((angular.isObject(options) && options.name && options.attr) || angular.isString(options) || angular.isArray(options) ) {
                    // get parser collections options
                    var ops = angular.isString(options) ? [{ name : attr.vwEvent, attr: 'event' }] : (angular.isArray(options) ? options: [options]);
                    // iterate in collection
                    for(var i = 0; i < ops.length; i++) {
                        // get op
                        var op = ops[i];
                        // define getter
                        var getter = $parse(op.name);
                        // value
                        var val = getter(views.event);
                        // check we have a formName defined
                        element.attr(op.attr, val ? val : attr.vwEvent);
                    }
                }
                else{
                    $log.error('invalid use of vwEvent');
                }
            }
        }
    }]);
});