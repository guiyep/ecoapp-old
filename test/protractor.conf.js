/**
 * Created by guiyep on 31/12/15.
 */
exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['integration-spec/dummyTestSpec.js']
};