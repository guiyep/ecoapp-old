var allTestFiles = [];
var TEST_REGEXP = /(spec|test)\.js$/i;

// Get a list of all the test files to include
Object.keys(window.__karma__.files).forEach(function(file) {
  if (TEST_REGEXP.test(file)) {
    allTestFiles.push(file);
  }

});

console.log(JSON.stringify(allTestFiles));

require.config({
    // Karma serves files under /base, which is the basePath from your config file
    baseUrl: '/base/Modules',

    // dynamically load all test files
    deps: allTestFiles,

    // paths to the libs
    paths: {
        domReady: '../node_modules/dom-ready/domReady',
        angular: '../node_modules/angular/angular',
        angularRoute: '../node_modules/angular-route/angular-route.min',
        angularSanitize: '../node_modules/angular-sanitize/angular-sanitize.min',
        jQuery: '../node_modules/jquery/dist/jquery.min',
        bootstrap: '../node_modules/bootstrap/dist/js/bootstrap.min',
        angularBootstrap: '../node_modules/angular-bootstrap-npm/dist/angular-bootstrap',
        angularFileUpload: '../node_modules/angular-file-upload/dist/angular-file-upload.min',
        moment: '../node_modules/moment/min/moment.min',
        jQueryUi: '../node_modules/jquery-ui/jquery-ui',
    },
    // requirejs shim
    shim: {
        jQuery : {
            exports: 'jQuery'
        },
        jQueryUi: {
            deps: ['jQuery']
        },
        fullPage: {
            deps: ['jQuery']
        },
        angular : {
            exports: 'angular'
        },
        angularRoute: {
            deps: ['angular']
        },
        angularSanitize: {
            deps: ['angular']
        },
        angularMocks: {
            deps: ['angular']
        },
        bootstrap: {
            deps: ['jQuery']
        },
        angularBootstrap: {
            deps: ['bootstrap','angular']
        },
        angularFileUpload: {
            deps: ['angular', 'bootstrap']
        },
        moment: {
            exports: 'moment'
        }
    },

    // we have to kickoff jasmine, as it is asynchronous
    callback: window.__karma__.start
});
