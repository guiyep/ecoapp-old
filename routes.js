/**
 * Created by Guille on 25/09/2014.
 */
define(['./app', 'angularRoute'], function (app) {
    'use strict';

    console.log('App Routes Loaded');

    return app.config(['$routeProvider', function ($routeProvider) {
        // define route for home
       $routeProvider.when('/home', {
            templateUrl: 'views/main/home.html'
        });
        $routeProvider.when('/register', {
            templateUrl: 'views/main/register.html'
        });
        $routeProvider.when('/login', {
            templateUrl: 'views/main/login.html'
        });
        $routeProvider.when('/tours', {
            templateUrl: 'views/main/requests.html'
        });
        $routeProvider.when('/create', {
            templateUrl: 'views/main/request.html'
        });
        $routeProvider.when('/profile', {
            templateUrl: 'views/main/profile.html'
        });
        $routeProvider.when('/inbox', {
            templateUrl: 'views/main/list.html'
        });
        $routeProvider.when('/yourlisting', {
            templateUrl: 'views/main/list.html'
        });
        $routeProvider.when('/wishes', {
            templateUrl: 'views/main/list.html'
        });
        // redirect to home if we do not fined the route
        $routeProvider.otherwise({
            redirectTo: '/home'
        });
    }]);
});