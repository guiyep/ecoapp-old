PROJECT STOPPE
--------------
this project was in an early stage.

GENERAL:
-------

Website to help people understand the impact of their usage of the world resources in a fun way.

README:
--------

* THIT PROJECT IS IN DEVELOPMENT FASE , I may have things without finish (most of them will be commented) * 

* CSS are basic, no responsive styles yet just structural( but responsiveness is taking into account in an API prospective). *

* each line is commented to have a better understanding of what is happening *

* no authentication is made yet, just get as for testing purpose *

* no animation yet set *

* for mobile will use in the future cordova to expose particularly mobile functionality and make app downloadable from appstore *

* Data in controllers is hardcoded for now, not yet saved or get from the database *

UNDERSTANDING
-------------

* This project quick off as 4 separate modules.  *

Suit -> No dependencies. 

	(this modules is in charge of the general things of the app, like languages, resources, screen and initialisation based on devices, etc.)
    (we use providers for each service initialisation, this way we can extend and add as many information as we need)

    ex: we have a provider that initialise languages in general and then we configure each of the languages we want to add. Same happens with devices.
	
Api -> Depends on Suit.

	(this is the core of the app, where all the core controls are (generic ones), controllers, directives, services will be place here that will be used in the rest of the app. )
	
Model -> No dependencies.

	(Here are the services that will connect to the Rest Api and save/update/delete/get entities (JSON representation that will be translated and saved))
	
Views -> Depends on Suit, Api, Model.

	(Here is placed all the UI, Business Logic of the App.)

Each modules has Controllers/Services/Directives/Controles that will be based on the module that they are placed.

	ex: if we have a controller in Api. This is a Controller that will be use as a generic controller for a particularly core control. This controll will be used by the View module (in this case, but can be used by any module that depend from it).
	ex2: If you have a directive in the Api, is a core control that will be used across the dependencies of him. Views depend on Api, means that will be able to use the directives of this module.
	ex3: a Controller of a view will have the Business logic for the app.
    ex4: if you have a directive in the view modules. Is a directive that only will be use as a control for a view that is something 100% custom and no generic.

	
TESTING
-------

* Each module is separate testable (we use Karma with Jasmine), with his own unit-test separate in files one for each unit test. Is all set up but we need to implement each one, we just have a dummy test in each module. *

* We have in the main directory a test folder where we place all the integration test (we use protractor for this). Is all set up but we need to implement each one, we just have a dummy test. *

KNOWN ISSUES
------------


* the JSON representation of the model, should be place as classes to reduce errors. *

* implement the rest of the Model *

* implement authentication *

* now days we can remove JQuery reference from angular dependencies and use pure Javascript *

* Implement deploy process with grunt. *